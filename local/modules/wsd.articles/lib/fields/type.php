<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Fields;

use wsd\Articles\Module;

/**
 * Class Type.
 *
 * @package WSD\Articles\Fields
 */
class Type
{
    /**
     * @var string Type code.
     */
    private $code;

    /**
     * @var string Type name.
     */
    private $name;

    /**
     * Type constructor.
     *
     * @param string $code
     *
     * @throws \WSD\Articles\Exceptions\ArticleListException
     */
    public function __construct(string $code)
    {
        $this->code = $code;
        $this->changeName();
    }

    /**
     * Change type name on create type object.
     *
     * @throws \WSD\Articles\Exceptions\ArticleListException
     */
    private function changeName()
    {
        $types = Module::getConfig('field_types');

        if (is_array($types) && count($types) > 0) {
            $types = array_column($types, 'LANG', 'CODE');

            $this->name = isset($types[$this->code]) ? $types[$this->code][strtoupper(LANGUAGE_ID)] : $this->code;
        } else {
            $this->name = $this->code;
        }
    }

    /**
     * Return type code.
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * Return type name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
}
