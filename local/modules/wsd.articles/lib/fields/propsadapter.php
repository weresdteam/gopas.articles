<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Fields;

use WSD\Articles\Exceptions\ArticleListException;
use WSD\Articles\Interfaces\iPropsAdapter;

/**
 * Class PropsAdapter.
 *
 * @package WSD\Articles\Fields
 */
class PropsAdapter implements iPropsAdapter
{
    /**
     * @var \CIBlockProperty|\CUserTypeEntity Property provider.
     */
    private $provider;

    /**
     * @var bool Section flag.
     */
    private $isSection = false;

    /**
     * PropsAdapter constructor.
     *
     * @param \CIBlockProperty|\CUserTypeEntity $provider
     *
     * @throws ArticleListException
     */
    public function __construct($provider)
    {
        if (
            !($provider instanceof \CIBlockProperty) &&
            !($provider instanceof \CUserTypeEntity)
        ) {
            throw new ArticleListException('Not supported provider');
        }

        if ($provider instanceof \CUserTypeEntity) {
            $this->isSection = true;
        }

        $this->provider = $provider;
    }

    /**
     * Return props list.
     *
     * @param array $filter
     *
     * @return array
     *
     * @throws ArticleListException
     */
    public function getProps(array $filter = []): array
    {
        if ($this->provider instanceof \CUserTypeEntity) {
            return $this->getUTProps($filter);
        }

        if ($this->provider instanceof \CIBlockProperty) {
            return $this->getIBProps($filter);
        }

        return [];
    }


    /**
     * Return iBlock props.
     *
     * @param array $filter
     *
     * @return array
     *
     * @throws ArticleListException
     */
    private function getIBProps(array $filter): array
    {
        $result = [];
        $filter = array_merge(['ACTIVE' => 'Y'], $filter);

        if (empty($filter['IBLOCK_ID'])) {
            throw new ArticleListException('Not set IBLOCK_ID');
        }

        $oProps = $this->provider::GetList([], $filter);

        while ($arProp = $oProps->GetNext()) {
            $result[] = [
                'NAME' => $arProp['NAME'],
                'MULTIPLE' => $arProp['MULTIPLE'] == 'Y' ? true : false,
                'REQUIRE' => $arProp['IS_REQUIRED'] == 'Y' ? true : false,
                'CODE' => 'PROPERTY_' . $arProp['CODE'],
                'TYPE' => 'STRING',
            ];
        }

        return $result;
    }

    /**
     * Return user type properties.
     *
     * @param array $filter
     *
     * @return array
     *
     * @throws ArticleListException
     */
    private function getUTProps(array $filter): array
    {
        $result = [];

        if (empty($filter['IBLOCK_ID'])) {
            throw new ArticleListException('Not set IBLOCK_ID');
        }

        $filter = array_merge(['ENTITY_ID' => 'IBLOCK_' . (int)$filter['IBLOCK_ID'] . '_SECTION'], $filter);

        if (empty($filter['LANG'])) {
            $filter['LANG'] = LANGUAGE_ID;
        }

        unset($filter['IBLOCK_ID']);

        $oProps = $this->provider::GetList([], $filter);

        while ($arProp = $oProps->GetNext()) {
            $result[] = [
                'NAME' => $arProp['EDIT_FORM_LABEL'] ?: $arProp['FIELD_NAME'],
                'MULTIPLE' => $arProp['MULTIPLE'] == 'Y' ? true : false,
                'CODE' => $arProp['FIELD_NAME'],
                'REQUIRE' => $arProp['MANDATORY'] == 'Y' ? true : false,
                'TYPE' => 'STRING',
            ];
        }

        return $result;
    }

    /**
     * Check section flag.
     *
     * @return bool
     */
    public function isSection(): bool
    {
        return $this->isSection;
    }
}
