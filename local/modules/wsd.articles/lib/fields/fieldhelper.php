<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Fields;

/**
 * Class FieldHelper.
 *
 * @package WSD\Articles\Fields
 */
class FieldHelper
{
    /**
     * Check iBlock element field on property type by field name.
     *
     * @param string $propName
     *
     * @return bool
     */
    public static function isProp(string $propName): bool
    {
        return preg_match('/^PROPERTY_/', $propName) === 1;
    }

    /**
     * Check iBlock section field on property type by field name.
     *
     * @param string $propName
     *
     * @return bool
     */
    public static function isUF(string $propName): bool
    {
        return preg_match('/^UF_/', $propName) === 1;
    }

    /**
     * Return clear property name by iBlock element property name.
     *
     * @param string $propName
     *
     * @return string
     */
    public static function getPropCode(string $propName): string
    {
        return static::isProp($propName) ? substr($propName, 9) : $propName;
    }
}
