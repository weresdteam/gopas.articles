<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Fields;

use wsd\Articles\Module;

/**
 * Class Types.
 *
 * @package WSD\Articles\Fields
 */
class Types
{
    /**
     * Return list existed field types.
     *
     * @return array
     *
     * @throws \WSD\Articles\Exceptions\ArticleListException
     */
    public static function getList()
    {
        $result = [];

        $list = Module::getConfig('field_types') ?: [];

        foreach ($list as $type) {
            $result[] = new Type($type['CODE']);
        }

        return $result;
    }
}
