<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Fields;

use CIBlockParameters;

/**
 * Class Field.
 *
 * @package WSD\Articles\Fields
 */
class Field
{
    /**
     * @var string Field name.
     */
    private $name;

    /**
     * @var string Field code.
     */
    private $code;

    /**
     * @var string Field type.
     */
    private $type;

    /**
     * @var mixed Field value.
     */
    private $value;

    /**
     * @var boolean Field require flag.
     */
    private $require;

    /**
     * @var boolean Field miltiple flag.
     */
    private $multiple;


    /**
     * Field constructor.
     *
     * @param string $code
     * @param string $type
     * @param bool $require
     * @param bool $multiple
     * @param string $value
     * @param string $name
     */
    public function __construct(
        string $code,
        string $type,
        bool $require = false,
        bool $multiple = false,
        $value = '',
        string $name =''
    ) {
        $this->code = $code;
        $this->type = $type;
        $this->require = $require;
        $this->multiple = $multiple;
        $this->value = $value;

        if (strlen($name) > 0 ) {
            $this->name = $name;
        } else {
            $this->changeName();
        }
    }

    /**
     * Return field code.
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * Return field name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Return field type.
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Return field value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Change field name on create field object.
     */
    private function changeName(): void
    {
        $name = '';

        if (FieldHelper::isProp($this->code)) {
            $oProp = \CIBlockProperty::GetByID(FieldHelper::getPropCode($this->code));
            $arProp = $oProp->GetNext();
            if ($arProp != false) {
                $name = $arProp['NAME'];
            }
        } elseif (FieldHelper::isUF($this->code)) {
            $oProp = \CUserTypeEntity::GetList([],['LANG' => LANGUAGE_ID, 'FIELD_NAME' => $this->code]);
            $arProp = $oProp->GetNext();
            if ($arProp != false) {
                $name = $arProp['EDIT_FORM_LABEL'];
            }
        } else {
            $arFieldsName = array_merge(
                CIBlockParameters::GetFieldCode('', '')['VALUES'],
                CIBlockParameters::GetSectionFieldCode('', '')['VALUES']
            );
            $name = $arFieldsName[$this->code];
        }

        $this->name = $name ?: $this->code;
    }

    /**
     * Convert object ot array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
          'name' => $this->getName(),
          'code' => $this->getCode(),
          'type' => $this->getType(),
          'require' => $this->getRequire(),
          'multiple' => $this->isMultiple(),
          'value' => $this->getValue(),
        ];
    }

    /**
     * Set field value.
     *
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    /**
     * Return require flag.
     *
     * @return boolean
     */
    public function getRequire(): bool
    {
        return $this->require;
    }

    /**
     * Return multiple flag.
     *
     * @return boolean
     */
    public function isMultiple(): bool
    {
        return $this->multiple;
    }
}
