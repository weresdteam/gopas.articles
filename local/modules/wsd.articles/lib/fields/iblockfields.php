<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Fields;

use Bitrix\Main\Loader;
use WSD\Articles\Interfaces\iPropsAdapter;
use wsd\Articles\Module;

/**
 * Class IBlockFields.
 *
 * @package WSD\Articles\Fields
 */
class IBlockFields
{
    /**
     * @var int iBlock id.
     */
    private $iblockId = null;

    /**
     * @var array List granted iblock fields.
     */
    private $iblockFields = [];

    /**
     * @var PropsAdapter Property adapter.
     */
    private $propsAdapter;

    /**
     * IBlockFields constructor.
     *
     * @param iPropsAdapter $propsAdapter
     * @param int $iblockId
     *
     * @throws \Bitrix\Main\LoaderException
     * @throws \WSD\Articles\Exceptions\ArticleListException
     */
    public function __construct(iPropsAdapter $propsAdapter, int $iblockId)
    {
        $this->iblockId = $iblockId;
        $this->propsAdapter = $propsAdapter;

        if ($this->propsAdapter->isSection()) {
            $fieldsFile = Module::getConfig('iblock_section_fields');
        } else {
            $fieldsFile = Module::getConfig('iblock_element_fields');
        }

        if (is_array($fieldsFile) && count($fieldsFile) > 0) {
            $this->iblockFields = $fieldsFile;
        }

        Loader::includeModule('iblock');
    }

    /**
     * Return list iblock fields.
     *
     * @return Fields[]
     *
     * @throws \WSD\Articles\Exceptions\ArticleListException
     */
    public function getIblockFields(): array
    {
        $result = [];

        $arProps = $this->propsAdapter->getProps(['IBLOCK_ID' => $this->iblockId]);
        $fields = array_merge($this->iblockFields, $arProps);

        if (count($fields) === 0) {
            $fields = Module::getConfig('default_iblock_field');
        }

        foreach ($fields as $field) {
            $result[] = new Field(
                $field['CODE'],
                $field['TYPE'],
                $field['REQUIRE'],
                $field['MULTIPLE'],
                '',
                $field['NAME'] ?? ''
            );
        }

        return $result;
    }
}
