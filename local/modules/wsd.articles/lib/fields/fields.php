<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Fields;

/**
 * Class Fields.
 *
 * @package WSD\Articles\Fields
 */
class Fields
{
    /**
     * Return list iblock fields.
     *
     * @param \CIBlockProperty|\CUserTypeEntity $provider
     * @param int $iblockId
     *
     * @return Fields[]
     *
     * @throws \Bitrix\Main\LoaderException
     * @throws \WSD\Articles\Exceptions\ArticleListException
     */
    public static function getList($provider, int $iblockId)
    {
        $fields = new IBlockFields(new PropsAdapter($provider), $iblockId);

        return $fields->getIblockFields();
    }
}
