<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Api\Methods;

use WSD\Articles\Abstracts\ApiMethod;
use WSD\Articles\Entity\Element;
use WSD\Articles\Entity\Section;

/**
 * Class ApiDELETE.
 *
 * @package WSD\Articles\Api\Methods
 */
class ApiDELETE extends ApiMethod
{
    /**
     * Delete section.
     *
     * @return string
     *
     * @throws \Bitrix\Main\ArgumentException
     */
    public function deleteSection()
    {
        $result = [
            'error' => true,
            'id' => 0
        ];

        $oSection = new Section((int)$this->oRequest->get('id'));

        if ($oSection->delete()) {
            $result['error'] = false;
        }

        return json_encode($result);
    }

    /**
     * Delete element.
     *
     * @return string
     *
     * @throws \Bitrix\Main\ArgumentException
     */
    public function deleteElement()
    {
        $result = [
            'error' => true,
            'id' => 0
        ];

        $oElement = new Element((int)$this->oRequest->get('id'));

        if ($oElement->delete()) {
            $result['error'] = false;
        }

        return json_encode($result);
    }
}
