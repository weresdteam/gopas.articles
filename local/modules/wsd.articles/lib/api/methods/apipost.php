<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Api\Methods;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use WSD\Articles\Abstracts\ApiMethod;
use WSD\Articles\Api\ApiHelper;
use WSD\Articles\Entity\Element;
use WSD\Articles\Entity\Section;
use WSD\Articles\Exceptions\ApiException;
use WSD\Articles\Fields\FieldHelper;

/**
 * Class ApiPOST
 *
 * @package WSD\Articles\Api\Methods
 */
class ApiPOST extends ApiMethod
{
    /**
     * @var int Section id by request.
     */
    private $sectionId = 0;

    /**
     * @var \WSD\Articles\Entity\Section Section entity created on self::sectionId.
     */
    private $oSection;

    /**
     * ApiPOST constructor.
     *
     * @param \Bitrix\Main\HttpRequest $request
     * @param array $rootSections
     * @param array $arParams
     *
     * @throws ApiException
     * @throws \Bitrix\Main\ArgumentException
     */
    public function __construct(\Bitrix\Main\HttpRequest $request, array $rootSections, array $arParams)
    {
        parent::__construct($request, $rootSections, $arParams);

        $this->sectionId = (int)$this->oRequest->get('section_id');

        if ($this->sectionId <= 0) {
            throw new ApiException(Loc::getMessage('WSD_A_ACP_E_SEC'));
        }

        $this->oSection = new Section($this->sectionId);
    }

    /**
     * Save element data.
     *
     * @return string
     *
     * @throws ApiException
     * @throws SystemException
     * @throws \Bitrix\Main\ArgumentException
     */
    public function saveElement()
    {
        $result = [
            'error' => true,
            'id' => 0
        ];

        if (empty($this->arParams['ELEMENT_FIELDS'])) {
            throw new ApiException(Loc::getMessage('WSD_A_ACP_E_ELEMENT_FIELDS'));
        }

        $elementFields = json_decode(htmlspecialchars_decode($this->arParams['ELEMENT_FIELDS']), true);

        if (!is_array($elementFields)) {
            throw new ApiException(Loc::getMessage('WSD_A_ACP_E_JSON_EL_FIELDS'));
        }

        $oElement = new Element((int)$this->oRequest->get('id'));

        $oElement->setField('IBLOCK_ID', (int)$this->oSection->getField('IBLOCK_ID'));
        $oElement->setField('IBLOCK_SECTION_ID', $this->oSection->getId());
        $arProperties = [];

        foreach ($elementFields as $field) {

            $isProp = FieldHelper::isProp($field['code']);
            $isRequire = (bool)$field['require'];
            $toSave = true;

            // блок проверки заполненности обязательных полей
            if (ApiHelper::isFileField($field['type'])) {
                $value = json_decode($this->oRequest->get($field['code']), true);

                $existFiles = false;

                foreach ($value as $arVal) {
                    if ($arVal['status'] >= 0) {
                        $existFiles = true;
                    }
                }

                if ($isRequire && !$existFiles) {
                    throw new ApiException(Loc::getMessage('WSD_A_ACP_E_REQ_EL_FIELD', ['#FIELD#' => $field['code']]));
                }

                if (in_array($field['code'], ['PREVIEW_PICTURE', 'DETAIL_PICTURE'])) {
                    $value = array_shift($value);

                    if ($value['status'] === 0) {
                        $value = $this->oRequest->getFile('FILE_' . $value['id']);

                        if (empty($value)) {
                            throw new ApiException(
                                Loc::getMessage('WSD_A_ACP_E_LOAD_FILE', ['#FIELD#' => $field['code']])
                            );
                        }
                    } elseif($value['status'] === -1) {
                        $value = ['del' => 'Y'];
                    } else {
                        $toSave = false;
                    }
                }
            } else {
                $value = (string)$this->oRequest->get($field['code']);

                if ($isRequire && strlen($value) === 0) {
                    throw new ApiException(Loc::getMessage('WSD_A_ACP_E_REQ_EL_FIELD', ['#FIELD#' => $field['code']]));
                }
            }

            if ($isProp) {
                $propCode = FieldHelper::getPropCode($field['code']);

                if (ApiHelper::isFileField($field['type'])) {

                    $filePropIdAssoc = [];
                    if ($oElement->getId() > 0) {
                        $oProps = $oElement->getProperties();
                        $oProp = $oProps[$propCode];
                        if (
                            $oProp['MULTIPLE'] == 'Y' &&
                            $oProp['PROPERTY_TYPE'] == 'F' &&
                            is_array($oProp['VALUE']) &&
                            is_array($oProp['PROPERTY_VALUE_ID'])
                        ) {
                            $filePropIdAssoc = array_combine($oProp['VALUE'], $oProp['PROPERTY_VALUE_ID']);
                        }
                    }

                    foreach ($value as $arVal) {
                        if ($arVal['status'] == 1) {
                            if ($filePropIdAssoc[$arVal['id']]) {
                                $arProperties[$propCode][$filePropIdAssoc[$arVal['id']]] = [
                                    'VALUE' => \CFile::GetFileArray($arVal['id']),
                                    'DESCRIPTION' => $arVal['description'],
                                ];
                            }
                        } elseif ($arVal['status'] == -1) {
                            if ($filePropIdAssoc[$arVal['id']]) {
                                $arProperties[$propCode][$filePropIdAssoc[$arVal['id']]] = [
                                    'VALUE' => ['del' => 'Y']
                                ];
                            }
                        } elseif($arVal['status'] == 0) {
                            $file = $this->oRequest->getFile('FILE_' . $arVal['id']);

                            if (empty($file)) {
                                throw new ApiException(
                                    Loc::getMessage('WSD_A_ACP_E_LOAD_FILE', ['#FIELD#' => $field['code']])
                                );
                            }

                            if (count($filePropIdAssoc) && !$arProperties[$propCode][max($filePropIdAssoc)]) {
                                $arProperties[$propCode][max($filePropIdAssoc)] = [];
                            }

                            $arProperties[$propCode][] = [
                                'VALUE' => $file,
                                'DESCRIPTION' => $arVal['description'],
                            ];
                        }
                    }

                    if (!(bool)$field['multiple']) {
                        $arProperties[$propCode] = array_shift($arProperties[$propCode]);
                    }
                } else {
                    $arProperties[$propCode] = $value;
                }

                $toSave = false;
            }

            if ($toSave) {
                $oElement->setField($field['code'], $value);

                if (in_array($field['code'], ['PREVIEW_TEXT', 'DETAIL_TEXT'])) {
                    $oElement->setField(
                        $field['code'] . '_TYPE',
                        (string)$this->oRequest->get($field['code'] . '_TYPE')
                    );
                }
            }
        }

        $oNewElement = $oElement->save();

        if ($oNewElement->getId() > 0) {
            $result['error'] = false;
            $result['id'] = $oNewElement->getId();

            if (!empty($arProperties)) {
                \CIBlockElement::SetPropertyValuesEx(
                    $oNewElement->getId(),
                    $oNewElement->getField('IBLOCK_ID'),
                    $arProperties
                );
            }
        }

        return json_encode($result);
    }

    /**
     * Save section data.
     *
     * @return string
     *
     * @throws ApiException
     * @throws SystemException
     * @throws \Bitrix\Main\ArgumentException
     */
    public function saveSection()
    {
        $result = [
            'error' => true,
            'id' => 0
        ];

        if (empty($this->arParams['SECTION_FIELDS'])) {
            throw new ApiException(Loc::getMessage('WSD_A_ACP_E_SECTION_FIELDS'));
        }

        $sectionFields = json_decode(htmlspecialchars_decode($this->arParams['SECTION_FIELDS']), true);

        if (!is_array($sectionFields)) {
            throw new ApiException(Loc::getMessage('WSD_A_ACP_E_JSON_SEC_FIELDS'));
        }

        $oSection = new Section((int)$this->oRequest->get('id'));
        $oSection->setField('IBLOCK_ID', (int)$this->oSection->getField('IBLOCK_ID'));
        $oSection->setField('IBLOCK_SECTION_ID', $this->oSection->getId());


        foreach ($sectionFields as $field) {
            $isRequire = (bool)$field['require'];
            $toSave = true;

            if (ApiHelper::isFileField($field['type'])) {
                $value = json_decode($this->oRequest->get($field['code']), true);

                $existFiles = false;

                foreach ($value as $arVal) {
                    if ($arVal['status'] >= 0) {
                        $existFiles = true;
                    }
                }

                if ($isRequire && !$existFiles) {
                    throw new ApiException(Loc::getMessage('WSD_A_ACP_E_REQ_SEC_FIELD', ['#FIELD#' => $field['code']]));
                }

                if (in_array($field['code'], ['PICTURE',])) {
                    $value = array_shift($value);

                    if ($value['status'] === 0) {
                        $value = $this->oRequest->getFile('FILE_' . $value['id']);

                        if (empty($value)) {
                            throw new ApiException(
                                Loc::getMessage('WSD_A_ACP_E_LOAD_FILE', ['#FIELD#' => $field['code']])
                            );
                        }
                    } elseif($value['status'] === -1) {
                        $value = ['del' => 'Y'];
                    } else {
                        $toSave = false;
                    }
                }

                if (FieldHelper::isUF($field['code'])) {
                    $files = [];
                    foreach ($value as $arVal) {
                        switch ($arVal['status']){
                            case 1:
                                $file = \CFile::MakeFileArray($arVal['id']);
                                $file['old_file'] = $arVal['id'];
                                $files[] = $file;
                                break;
                            case -1:
                                $files[] = ['del' => 'Y', 'old_file' => $arVal['id']];
                                break;
                            case 0:
                                $f = $this->oRequest->getFile('FILE_' . $arVal['id']);

                                if (empty($f)) {
                                    throw new ApiException(
                                        Loc::getMessage('WSD_A_ACP_E_LOAD_FILE', ['#FIELD#' => $field['code']])
                                    );
                                }
                                $files[] = $f;
                                break;
                        }
                    }

                    if (count($files)) {
                        $value = $files;
                    } else {
                        $toSave = false;
                    }

                    if (!$field['multiple']) {
                        $value = array_shift($files);
                    }
                }
            } else {
                $value = (string)$this->oRequest->get($field['code']);

                if ($isRequire && strlen($value) === 0) {
                    throw new ApiException(Loc::getMessage('WSD_A_ACP_E_REQ_SEC_FIELD', ['#FIELD#' => $field['code']]));
                }
            }

            if ($toSave) {
                $oSection->setField($field['code'], $value);

                if ($field['code'] == 'DESCRIPTION') {
                    $oSection->setField('DESCRIPTION_TYPE', (string)$this->oRequest->get('DESCRIPTION_TYPE'));
                }
            }
        }

        $oNewSection = $oSection->save();

        if ($oNewSection->getId() > 0) {
            $result['error'] = false;
            $result['id'] = $oNewSection->getId();
        }

        return json_encode($result);
    }
}
