<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Api\Methods;

use Bitrix\Main\Localization\Loc;
use WSD\Articles\Abstracts\ApiMethod;
use WSD\Articles\Api\ApiHelper;
use WSD\Articles\Entity\Section;
use WSD\Articles\Entity\Element;
use WSD\Articles\Exceptions\ApiException;
use WSD\Articles\Fields\Field;
use WSD\Articles\Fields\FieldHelper;

/**
 * Class ApiGET.
 *
 * @package WSD\Articles\Api\Methods
 */
class ApiGET extends ApiMethod
{
    /**
     * Return tree sections.
     *
     * Return section fields and him first section child.
     *
     * @return string
     *
     * @throws ApiException
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function getSectionTree()
    {
        $sectionId = (int)$this->oRequest->get('section_id');

        $result = [
            'error' => false,
            'msg' => '',
            'sections' => [],
            'elements' => [],
        ];

        if ($sectionId > 0) {

            $oRootSection = new Section($sectionId);
            $arRootSubSections = $oRootSection->getSubSections();
            $arRootElements = $oRootSection->getElements();
            $isMainSection = in_array($oRootSection->getId(), $this->rootSections);

            $result['sections'][$oRootSection->getId()] = [
                'id' => $oRootSection->getId(),
                'name' => in_array($oRootSection->getId(), $this->rootSections)
                    ? $this->arParams['ALIAS_ROOT_SECTION']
                    : trim($oRootSection->getField('NAME')),
                'parent' => $oRootSection->getParentId(),
                'sections' => array_map(function ($item) {
                    return $item->getId();
                }, $arRootSubSections),
                'elements' => array_map(function ($item) {
                    return $item->getId();
                }, $arRootElements),
                'actions' => [
                    'add_elements' => true,
                    'add_sections' => true,
                    'edit' => !$isMainSection,
                    'delete' => $isMainSection ? false : count($oRootSection->getElements()) === 0,
                ],
                'state' => [
                    'hide' => false,
                    'padding' => true,
                ]
            ];

            foreach ($arRootSubSections as $oSection) {
                $result['sections'][$oSection->getId()] = [
                    'id' => $oSection->getId(),
                    'name' => in_array($oSection->getId(), $this->rootSections)
                        ? $this->arParams['ALIAS_ROOT_SECTION']
                        : trim($oSection->getField('NAME')),
                    'parent' => $oRootSection->getId(),
                    'sections' => array_map(function ($item) {
                        return $item->getId();
                    }, $oSection->getSubSections()),
                    'elements' => array_map(function ($item) {
                        return $item->getId();
                    }, $oSection->getElements()),
                    'actions' => [
                        'add_elements' => true,
                        'add_sections' => true,
                        'edit' => true,
                        'delete' => count($oSection->getElements()) === 0,
                    ],
                    'state' => [
                        'hide' => false,
                        'padding' => true,
                    ]
                ];
            }

            foreach ($arRootElements as $oElement) {
                $result['elements'][$oElement->getId()] = [
                    'id' => $oElement->getId(),
                    'name' => trim($oElement->getField('NAME')),
                    'parent' => $oRootSection->getId(),
                    'actions' => [
                        'edit' => true,
                        'delete' => true,
                    ],
                    'state' => [
                        'hide' => false,
                        'padding' => true,
                    ]
                ];
            }
        } else {
            throw new ApiException(Loc::getMessage('WSD_A_ACG_E_SEC'));
        }

        return json_encode($result);
    }

    /**
     * Return values section fields.
     *
     * @return string
     *
     * @throws ApiException
     * @throws \Bitrix\Main\ArgumentException
     */
    public function getSectionFields()
    {
        $sectionId = (int)$this->oRequest->get('id');

        if (empty($this->arParams['SECTION_FIELDS'])) {
            throw new ApiException(Loc::getMessage('WSD_A_ACG_E_SECTION_FIELDS'));
        }

        $sectionFields = json_decode(htmlspecialchars_decode($this->arParams['SECTION_FIELDS']), true);

        if (!is_array($sectionFields)) {
            throw new ApiException(Loc::getMessage('WSD_A_ACG_E_JSON_SEC_FIELDS'));
        }

        $result = [
            'error' => false,
            'msg' => '',
            'id' => $sectionId,
            'fields' => [],
        ];

        if ($sectionId >= 0) {
            $oSection = new Section($sectionId);

            // сортировка по полю sort
            usort($sectionFields, function ($a, $b) {
                $tmpA = (int)$a['sort'];
                $tmpB = (int)$b['sort'];

                return $tmpA == $tmpB ? 0 : $tmpA < $tmpB ? -1 : 1;
            });

            foreach ($sectionFields as $field) {

                $oField = new Field(
                    $field['code'],
                    $field['type'],
                    $field['require'],
                    $field['multiple'],
                    !$sectionId ? '' : $oSection->getField($field['code'])
                );

                $arField = $oField->toArray();

                if ($field['code'] == 'DESCRIPTION') {
                    $arField['text_type'] = !$sectionId ? 'text' : $oSection->getField('DESCRIPTION_TYPE');
                }

                if (ApiHelper::isFileField($arField['type'])) {
                    if (!is_array($arField['value'])) {
                        $arField['value'] = [(int)$arField['value']];
                    }
                    $arField['value'] = array_filter($arField['value']);

                    $arField['value'] = ApiHelper::getFiles($arField['value']);

                    foreach ($arField['value'] as &$valFile) {
                        $valFile['status'] = 1;
                        $valFile['type'] = 'file';
                    }
                }

                $result['fields'][] = $arField;
            }
        } else {
            throw new ApiException(Loc::getMessage('WSD_A_ACG_E_ID'));
        }

        return json_encode($result);
    }

    /**
     * Return values element fields.
     *
     * @return string
     *
     * @throws ApiException
     * @throws \Bitrix\Main\ArgumentException
     */
    public function getElementFields()
    {
        $elementId = (int)$this->oRequest->get('id');

        if (empty($this->arParams['ELEMENT_FIELDS'])) {
            throw new ApiException(Loc::getMessage('WSD_A_ACG_E_ELEMENT_FIELDS'));
        }

        $elementFields = json_decode(htmlspecialchars_decode($this->arParams['ELEMENT_FIELDS']), true);

        if (!is_array($elementFields)) {
            throw new ApiException(Loc::getMessage('WSD_A_ACG_E_JSON_EL_FIELDS'));
        }

        $result = [
            'error' => false,
            'msg' => '',
            'id' => $elementId,
            'fields' => [],
        ];

        if ($elementId >= 0) {
            $oElement = new Element($elementId);

            // сортировка по полю sort
            usort($elementFields, function ($a, $b) {
                $tmpA = (int)$a['sort'];
                $tmpB = (int)$b['sort'];

                return $tmpA == $tmpB ? 0 : $tmpA < $tmpB ? -1 : 1;
            });

            foreach ($elementFields as $field) {
                if (FieldHelper::isProp($field['code'])) {
                    if (!$elementId) {
                        $value = '';
                    } else {
                        $oProps = $oElement->getProperties();
                        $value = $oProps[FieldHelper::getPropCode($field['code'])]['VALUE'];
                    }

                } else {
                    $value = !$elementId ? '' : $oElement->getField($field['code']);
                }

                $oField = new Field(
                    $field['code'],
                    $field['type'],
                    $field['require'],
                    $field['multiple'],
                    $value
                );

                $arField = $oField->toArray();

                if ($field['code'] == 'PREVIEW_TEXT') {
                    $arField['text_type'] = !$elementId ? 'text' : $oElement->getField('PREVIEW_TEXT_TYPE');
                }

                if ($field['code'] == 'DETAIL_TEXT') {
                    $arField['text_type'] = !$elementId ? 'text' : $oElement->getField('DETAIL_TEXT_TYPE');
                }

                if (ApiHelper::isFileField($arField['type'])) {
                    if (!is_array($arField['value'])) {
                        $arField['value'] = [(int)$arField['value']];
                    }
                    $arField['value'] = array_filter($arField['value']);

                    $arField['value'] = ApiHelper::getFiles($arField['value']);

                    $descriptions = [];
                    if (!empty($oProps)) {
                        $oProp = $oProps[FieldHelper::getPropCode($field['code'])];
                        if (!empty($oProp) && is_array($oProp['DESCRIPTION']) && !empty($oProp['DESCRIPTION'])) {
                            $descriptions = array_combine($oProp['VALUE'], $oProp['DESCRIPTION']);
                        }
                    }

                    foreach ($arField['value'] as &$valFile) {
                        $valFile['status'] = 1;
                        $valFile['type'] = 'file';
                        $valFile['description'] = '';

                        if ($descriptions[$valFile['id']]) {
                            $valFile['description'] = $descriptions[$valFile['id']];
                        }
                    }
                }

                $result['fields'][] = $arField;
            }
        } else {
            throw new ApiException(Loc::getMessage('WSD_A_ACG_E_ID'));
        }

        return json_encode($result);
    }
}
