<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Api;

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use WSD\Articles\Exceptions\ApiException;

/**
 * Class ApiController
 *
 * @package WSD\Articles\Api
 */
class ApiController
{
    /**
     * @var \Bitrix\Main\HttpRequest Request object.
     */
    private $oRequest;

    /**
     * @var \Bitrix\Main\HttpResponse Response object.
     */
    private $oResponse;

    /**
     * @var string Key in HTTP params with API action name.
     */
    private $actionKey = 'action';

    /**
     * @var string Prefix for API action classes.
     */
    private $apiClassPrefix = 'Api';

    /**
     * @var string Type returned contents.
     */
    private $apiContentType = 'application/json';

    /**
     * @var int[] List root srctions
     */
    private $rootSections = [];

    /**
     * @var array Component parameters.
     */
    private $arParams = [];

    /**
     * ApiController constructor.
     *
     * @param $arParams array
     */
    public function __construct(array $arParams)
    {
        try {
            if (!is_array($arParams['ROOT_SECTIONS']) || count($arParams['ROOT_SECTIONS']) <= 0) {
                throw new ApiException(Loc::getMessage('WSD_A_AC_E_SEC_ID'));
            }

            $this->rootSections = $arParams['ROOT_SECTIONS'];
            $this->arParams = $arParams;

            $this->oRequest = Application::getInstance()->getContext()->getRequest();
            $this->oResponse = Application::getInstance()->getContext()->getResponse();
        } catch (\Exception $e) {
            $this->sendError();
        }
    }

    /**
     * Run API.
     */
    public function run()
    {
        global $USER;

        try {
            if (!$USER->IsAuthorized()) {
                throw new ApiException(Loc::getMessage('WSD_A_AC_E_USER_UNAUTHORIZE'));
            }

            if (!check_bitrix_sessid()) {
                throw new ApiException(Loc::getMessage('WSD_A_AC_E_CSRF'));
            }

            $sectionId = (int)$this->oRequest->get('section_id');
            if (!$this->isHaveAccess($sectionId)) {
                throw new ApiException(
                    Loc::getMessage('WSD_A_AC_E_SEC_DENIDE', ['#SEC#' => $sectionId, '#USER#' => $USER->GetID()])
                );
            }

            $apiClassName = $this->getApiClassName();

            if (!class_exists($apiClassName)) {
                throw new ApiException(Loc::getMessage('WSD_A_AC_E_WRONG_CLASS', ['#CLASS#' => $apiClassName]));
            } else {
                $oApiClass = new $apiClassName($this->oRequest, $this->rootSections, $this->arParams);
            }

            $apiMethodName = $this->getApiMethodName();

            if (!method_exists($apiClassName, $apiMethodName)) {
                throw new ApiException(
                    Loc::getMessage(
                        'WSD_A_AC_E_WRONG_METHOD',
                        ['#CLASS#' => $apiClassName, '#METHOD#' => $apiMethodName]
                    )
                );
            }

            $this->oResponse->addHeader('Content-type', $this->apiContentType);
            $this->oResponse->setContent($oApiClass->{$apiMethodName}());
            $this->oResponse->send();
            exit();

        } catch (ApiException $e) {
            $this->sendError($USER->IsAdmin() ? $e->getMessage() : '');
        } catch (\Exception $e) {
            $this->sendError($e->getMessage());
        }
    }

    /**
     * Send API errors.
     *
     * @param string $msg
     */
    private function sendError($msg = '')
    {
        $msg = trim($msg);
        header('Content-type: ' . $this->apiContentType);
        exit(json_encode([
            'error' => true,
            'msg' => strlen($msg) > 0 ? $msg : Loc::getMessage('WSD_A_AC_ERROR')
        ]));
    }

    /**
     * Return API method name by incoming action code.
     *
     * @return string
     */
    private function getApiMethodName(): string
    {
        $action = (int)$this->oRequest->get($this->actionKey);

        switch ($action) {
            case 1:
                $method = 'getSectionTree';
                break;
            case 2:
                $method = 'getSectionFields';
                break;
            case 3:
                $method = 'getElementFields';
                break;
            case 4:
                $method = 'deleteSection';
                break;
            case 5:
                $method = 'deleteElement';
                break;
            case 6:
                $method = 'saveSection';
                break;
            case 7:
                $method = 'saveElement';
                break;
            case 8:
                $method = 'getFiles';
                break;
            default:
                $method = '';
                break;
        }

        return $method;
    }

    /**
     * Return API class name.
     *
     * @return string
     */
    private function getApiClassName(): string
    {
        return __NAMESPACE__ .
            '\\Methods\\' .
            $this->apiClassPrefix . strtoupper(trim($this->oRequest->getRequestMethod()));
    }

    /**
     * Checks access to the requested section for the current user.
     *
     * @param int $sectionId
     *
     * @return bool
     */
    private function isHaveAccess(int $sectionId): bool
    {
        $isHave = false;

        if (in_array($sectionId, $this->rootSections)) {
            $isHave = true;
        } elseif (count($this->rootSections) > 0 && $sectionId > 0) {
            $ids = [];
            $oResult = \CIBlockSection::GetNavChain(0, $sectionId, ['ID']);

            while ($arFields = $oResult->GetNext()) {
                $ids[] = (int)$arFields['ID'];
            }

            $isHave = count(array_intersect($this->rootSections, $ids)) > 0;
        }

        return $isHave;
    }
}
