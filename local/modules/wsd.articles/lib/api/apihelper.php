<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Api;

use COption;

/**
 * Class ApiHelper.
 *
 * @package WSD\Articles\Api
 */
class ApiHelper
{
    /**
     * Check field on type of file type.
     *
     * @param string $fieldName
     * @return bool
     */
    public static function isFileField(string $fieldName): bool
    {
        return in_array($fieldName, ['FILE', 'IMAGE']);
    }

    /**
     * Return files fields.
     *
     * @param array $fileIds
     * @return array
     */
    public static function getFiles(array $fileIds): array
    {
        $result = [];

        if (count($fileIds) > 0) {
            $uploadDir = '/' . trim(COption::GetOptionString('main', 'upload_dir', 'upload'), '/');

            foreach ($fileIds as $fileId) {
                $oFile = \CFile::GetByID($fileId);

                if ($arFile = $oFile->Fetch()) {
                    $result[] = [
                        'id' => $arFile['ID'],
                        'name' => $arFile['ORIGINAL_NAME'],
                        'description' => $arFile['DESCRIPTION'],
                        'path' => implode('/', [$uploadDir, $arFile['SUBDIR'], $arFile['FILE_NAME'],]),
                    ];
                }
            }
        }

        return $result;
    }
}
