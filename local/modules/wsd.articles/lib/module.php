<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace wsd\Articles;

use WSD\Articles\Exceptions\ArticleListException;

/**
 * Class Module.
 *
 * @package wsd\Articles
 */
class Module
{
    /**
     * @var array List module configurations.
     */
    private static $config = [];

    /**
     * @var array List module containers.
     */
    private static $container = [];

    /**
     * Return module container by name.
     *
     * @param string $name
     *
     * @return mixed
     *
     * @throws ArticleListException
     */
    public static function getContainer(string $name)
    {
        self::loadConfig();

        if (!isset(self::$container[$name])) {
            $containers = self::getConfig('containers');

            if ($containers === null) {
                throw new ArticleListException('Not set containers part in config');
            }

            if (!class_exists($containers[$name])) {
                throw new ArticleListException('Not found container ' . $name);
            }

            self::$container[$name] = new $containers[$name];
        }

        return self::$container[$name];
    }

    /**
     * Return module configuration by name.
     *
     * @param string $name
     *
     * @return null|string|array
     *
     * @throws ArticleListException
     */
    public static function getConfig(string $name)
    {
        self::loadConfig();

        $config = &self::$config[$name];

        if (isset($config)) {
            if (is_string($config) && preg_match('/\.json$/', self::$config[$name]) !== false) {
                $config = self::parseJson(self::getModuleRootPath() . $config);
            }

            return $config;
        }
        return null;
    }

    /**
     * Load configuration.
     *
     * @throws ArticleListException
     */
    private static function loadConfig()
    {
        if (empty(self::$config)) {
            $moduleRootPath = self::getModuleRootPath();
            $configFile = $moduleRootPath . '/options/config.json';

            if (!file_exists($configFile)) {
                throw new ArticleListException('Not found file configuration');
            }

            $config = json_decode(file_get_contents($configFile), true);

            if ($config === false || $config === null) {
                throw new ArticleListException('Bad file configuration');
            }

            $config['module_root_path'] = $moduleRootPath;

            self::$config = $config;
        }
    }

    /**
     * Return module root path.
     *
     * @return string
     */
    private static function getModuleRootPath(): string
    {
        return realpath(__DIR__ . '/..');
    }


    /**
     * Return decoded content from json file.
     *
     * @param string $file
     *
     * @return array|null
     */
    private static function parseJson(string $file): ?array
    {
        if (file_exists($file)) {
            $config = json_decode(file_get_contents($file), true);

            if (!empty($config)) {
                return $config;
            }
        }

        return [];
    }
}
