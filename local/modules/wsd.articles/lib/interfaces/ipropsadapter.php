<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Interfaces;

/**
 * Interface iPropsAdapter.
 *
 * @package WSD\Articles\Interfaces
 */
interface iPropsAdapter
{
    /**
     * Return props list.
     *
     * @param array $filter
     *
     * @return array
     */
    public function getProps(array $filter = []): array;
}
