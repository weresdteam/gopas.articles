<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Exceptions;

/**
 * Class ApiException.
 *
 * Exception class for view API errors.
 *
 * @package WSD\Articles\Exceptions
 */
class ApiException extends ArticleListException
{

}
