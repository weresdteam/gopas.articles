<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Exceptions;

use Exception;
use Throwable;

/**
 * Class ArticleListException.
 *
 * Main module exception class.
 *
 * @package WSD\Articles\Exceptions
 */
class ArticleListException extends Exception
{
    /**
     * ArticleListException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        file_put_contents(__DIR__ . '/ErrorLog.log', date('d-m-Y H:i:s | ') . $message . PHP_EOL, FILE_APPEND);

        parent::__construct($message, $code, $previous);
    }
}
