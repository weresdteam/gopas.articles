<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Exceptions;

/**
 * Class FieldsException.
 *
 * Exception class for view field errors.
 *
 * @package WSD\Articles\Exceptions
 */
class FieldsException extends ArticleListException
{

}
