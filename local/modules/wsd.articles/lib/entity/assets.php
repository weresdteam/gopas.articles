<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Entity;

use CJSCore;

/**
 * Class Assets.
 *
 * @package WSD\Articles\entity
 */
class Assets
{
    /**
     * @var array List JS libs.
     */
    public static $jsLibs = [
        'wsd_vue' => [
            'js' => 'modules/wsd.articles/assets/js/vue/%svue.js',
        ],
        'wsd_vuex' => [
            'js' => 'modules/wsd.articles/assets/js/vuex/vuex.min.js',
        ],
        'wsd_vue_router' => [
            'js' => 'modules/wsd.articles/assets/js/vue_router/vue-router.js',
        ],
        'trumbowyg' => [
            'js' => 'modules/wsd.articles/assets/js/trumbowyg/trumbowyg.min.js',
            'rel' => ['jquery'],
            'css' => 'modules/wsd.articles/assets/js/trumbowyg/ui/trumbowyg.min.css',
        ],
    ];

    /**
     * Registers JS libs.
     */
    public static function regJSLibs()
    {
        global $USER;

        $prefix = $USER->IsAdmin() ? 'dev.' : '';

        foreach (self::$jsLibs as $libName => $libOption) {
            if (!empty($libOption['js'])) {

                if ($libName === 'wsd_vue') {
                    $libOption['js'] = sprintf($libOption['js'], $prefix);
                }

                $libOption['js'] = getLocalPath($libOption['js']);

                if (!empty($libOption['css'])) {
                    $libOption['css'] = getLocalPath($libOption['css']);
                }

                CJSCore::RegisterExt($libName, $libOption);
            }
        }
    }
}
