<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Entity;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\SystemException;
use WSD\Articles\abstracts\Entity;

/**
 * Class Section.
 *
 * @package WSD\Articles\Entity
 */
class Section extends Entity
{
    /**
     * Section constructor.
     *
     * @param $id
     * @throws ArgumentException
     */
    public function __construct($id)
    {
        $this->id = (int)$id;

        if ($this->id < 0) {
            throw new ArgumentException('Invalid section id: ' . $this->getId());
        }
    }

    /**
     * Return subsection of the current section.
     *
     * @param array $arOrder
     *
     * @return Section[]
     *
     * @throws ArgumentException
     * @throws SystemException
     */
    public function getSubSections($arOrder = [])
    {
        $result = [];

        $oResult = \CIBlockSection::GetList($arOrder, ['SECTION_ID' => $this->getId()], false, ['ID']);

        while ($arFields = $oResult->GetNext()) {
            $id = (int)$arFields['ID'];

            if ($id > 0) {
                $result[] = new static($id);
            }
        }

        return $result;
    }

    /**
     * Return elements of the current section.
     *
     * @param array $arOrder
     *
     * @return Element[]
     *
     * @throws ArgumentException
     * @throws SystemException
     */
    public function getElements($arOrder = [])
    {
        $result = [];

        $oResult = \CIBlockElement::GetList($arOrder, ['SECTION_ID' => $this->getId()], false, false, ['ID']);

        while ($arFields = $oResult->GetNext()) {
            $id = (int)$arFields['ID'];

            if ($id > 0) {
                $result[] = new Element($id);
            }
        }

        return $result;
    }

    /**
     * Return bitrix db element object.
     *
     * @return \_CIBElement|array
     *
     * @throws SystemException
     */
    protected function getQuery()
    {
        if (empty($this->oQuery)) {
            $oResult = \CIBlockSection::GetList(
                [],
                ['ID' => $this->id, 'IBLOCK_ID' => self::getIBlockId($this->id)],
                false,
                ['*', 'UF_*']
            );

            if (!($this->oQuery = $oResult->GetNextElement())) {
                throw new SystemException(sprintf('Section with id: %u not found', $this->getId()));
            }
        }

        return $this->oQuery;
    }

    /**
     * Return id parent section.
     *
     * @return int
     */
    public function getParentId()
    {
        $ids = [];
        $ibId = (int)$this->getField('IBLOCK_ID');
        $oResult = \CIBlockSection::GetNavChain($ibId, $this->getId(), ['ID']);

        while ($arFields = $oResult->GetNext()) {
            $ids[] = (int)$arFields['ID'];
        }

        // последний элемент - текущий, нам нужен предпоследний
        array_pop($ids); // уберем последний

        return (int)array_pop($ids);
    }

    /**
     * Save current section.
     *
     * @return $this|static
     *
     * @throws ArgumentException
     * @throws SystemException
     */
    public function save()
    {
        $oIBlockSection = new \CIBlockSection();

        if ($this->id > 0) {
            if (!$oIBlockSection->Update($this->id, $this->arFields)) {
                throw new SystemException($oIBlockSection->LAST_ERROR);
            }
        } else {
            $id = $oIBlockSection->Add($this->arFields);

            if ($id == false) {
                throw new SystemException($oIBlockSection->LAST_ERROR);
            }

            return new static($id);
        }

        return $this;
    }

    /**
     * Delete current section.
     *
     * @return array|bool|\CDBResult
     */
    public function delete()
    {
        return \CIBlockSection::Delete($this->getId());
    }

    /**
     * Return iBlock id by section id.
     *
     * @param int $secId
     *
     * @return int
     */
    protected static function getIBlockId($secId)
    {
        $oIBSec = \CIBlockSection::GetByID($secId);
        $arFields = $oIBSec->GetNext();

        return $arFields ? (int)$arFields['IBLOCK_ID'] : 0;
    }
}
