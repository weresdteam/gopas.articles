<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Entity;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\SystemException;
use WSD\Articles\abstracts\Entity;

/**
 * Class Element.
 *
 * @package WSD\Articles\Entity
 */
class Element extends Entity
{
    /**
     * Element constructor.
     *
     * @param $id
     *
     * @throws ArgumentException
     */
    public function __construct($id)
    {
        $this->id = (int)$id;

        if ($this->id < 0) {
            throw new ArgumentException('Invalid element id: ' . $this->getId());
        }
    }

    /**
     * Return bitrix db element object.
     *
     * @return \_CIBElement|array
     *
     * @throws SystemException
     */
    protected function getQuery()
    {
        if (empty($this->oQuery)) {
            $oResult = \CIBlockElement::GetList([], ['ID' => $this->getId()]);

            if (!($this->oQuery = $oResult->GetNextElement())) {
                throw new SystemException(sprintf('Element with id: %u not found', $this->getId()));
            }
        }

        return $this->oQuery;
    }

    /**
     * Save current element.
     *
     * @return $this|static
     *
     * @throws ArgumentException
     * @throws SystemException
     */
    public function save()
    {
        $oIBlockElement = new \CIBlockElement();

        if ($this->id > 0) {
            if (!$oIBlockElement->Update($this->id, $this->arFields, false, true, true)) {
                throw new SystemException($oIBlockElement->LAST_ERROR);
            }
        } else {
            $id = $oIBlockElement->Add($this->arFields, false, true, true);

            if ($id == false) {
                throw new SystemException($oIBlockElement->LAST_ERROR);
            }

            return new static($id);
        }

        return $this;
    }

    /**
     * Delete current element.
     *
     * @return bool
     */
    public function delete()
    {
        return \CIBlockElement::Delete($this->getId());
    }
}
