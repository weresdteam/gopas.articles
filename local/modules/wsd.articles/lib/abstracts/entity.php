<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Abstracts;

/**
 * Class Entity.
 *
 * @package WSD\Articles\Abstracts
 */
abstract class Entity
{
    /**
     * @var int Entity id.
     */
    protected $id;

    /**
     * @var \_CIBElement Bitrix db element object.
     */
    protected $oQuery;

    /**
     * @var array List fields.
     */
    protected $arFields;

    /**
     * @var array List properties.
     */
    protected $arProperties;

    /**
     * Return list fields.
     *
     * @return mixed
     */
    public function getFields()
    {
        return $this->arFields ?: $this->arFields = $this->getQuery()->GetFields();
    }

    /**
     * Return field by name.
     *
     * @param $fieldName
     * @param string $default
     *
     * @return string
     */
    public function getField($fieldName, $default = '')
    {
        return $this->getFields()[$fieldName] ?: $default;
    }

    /**
     * Set field value.
     *
     * @param $fieldName
     * @param string|int $value
     */
    public function setField($fieldName, $value = '')
    {
        $this->arFields[$fieldName] = $value;
    }

    /**
     * Return property list.
     *
     * @param bool $arOrder
     * @param array $arFilter
     *
     * @return array
     */
    public function getProperties($arOrder = false, $arFilter = [])
    {
        return $this->arProperties ?: $this->arProperties = $this->getQuery()->GetProperties($arOrder, $arFilter);
    }

    /**
     * Return entity id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \_CIBElement $this->oQuery
     */
    abstract protected function getQuery();

    /**
     * @return static
     */
    abstract public function save();

    /**
     * @return boolean
     */
    abstract public function delete();
}
