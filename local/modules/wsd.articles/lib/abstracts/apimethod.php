<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

namespace WSD\Articles\Abstracts;

/**
 * Class ApiMethod.
 *
 * @package WSD\Articles\Abstracts
 */
abstract class ApiMethod
{
    /**
     * @var \Bitrix\Main\HttpRequest Request object.
     */
    protected $oRequest;

    /**
     * @var array List root sections.
     */
    protected $rootSections = [];

    /**
     * @var array Component parameters.
     */
    protected $arParams = [];

    /**
     * ApiGET constructor.
     *
     * @param \Bitrix\Main\HttpRequest $request
     * @param array $rootSections
     * @param array $arParams
     */
    public function __construct(\Bitrix\Main\HttpRequest $request, array $rootSections, array $arParams)
    {
        $this->oRequest = $request;
        $this->rootSections = $rootSections;
        $this->arParams = $arParams;
    }
}
