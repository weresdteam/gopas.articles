<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

/**
 * Class WSDArticleListComponent
 */
class WSDArticleListComponent extends CBitrixComponent
{
    /**
     * Changes on prepare component param.
     *
     * @param $arParams
     *
     * @return array
     *
     * @throws \Bitrix\Main\LoaderException
     */
    public function onPrepareComponentParams($arParams)
    {
        if (!Loader::includeModule('iblock')) {
            $this->addError(Loc::getMessage('WSD_ASL_E_MODULE_IBLOCK'));
        }

        $arParams['QUERY_ASYNC'] = $arParams['QUERY_ASYNC'] == 'Y';
        $arParams['IBLOCK_ID'] = (int)$arParams['IBLOCK_ID'];
        $arParams['ALIAS_ROOT_SECTION'] = (string)$arParams['ALIAS_ROOT_SECTION'];

        if (empty($arParams['USER_FIELD'])) {
            $this->addError(Loc::getMessage('WSD_ASL_E_USER_FIELD'));
        }

        if (empty($arParams['SECTION_FIELD'])) {
            $this->addError(Loc::getMessage('WSD_ASL_E_SECTION_FIELD'));
        }

        if ($arParams['IBLOCK_ID'] <= 0) {
            $this->addError(Loc::getMessage('WSD_ASL_E_IBLOCK_ID'));
        }

        return $arParams;
    }

    /**
     * Execute component.
     *
     * @return mixed|void
     *
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        Loader::includeModule('wsd.articles');

        $this->arResult['ROOT_SECTIONS'] = $this->getRootSections();

        if (count($this->arResult['ROOT_SECTIONS']) <= 0) {
            $this->addError(Loc::getMessage('WSD_ASL_E_ROOT_SECTIONS'));
        }

        try {
            if ($this->isAjax()) {
                $GLOBALS['APPLICATION']->RestartBuffer();
                $oApi = new \WSD\Articles\Api\ApiController(array_merge($this->arParams, $this->arResult));
                $oApi->run();
            }
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
        }

        $this->includeComponentTemplate();
    }

    /**
     * Add error to list.
     *
     * @param $message
     */
    private function addError($message)
    {
        $this->arResult['ERRORS'][] = trim($message);
    }

    /**
     * Check on has error.
     *
     * @return bool
     */
    private function hasError()
    {
        return count($this->arResult['ERRORS']) > 0;
    }

    /**
     * Check on ajax request.
     *
     * @return bool
     *
     * @throws \Bitrix\Main\SystemException
     */
    protected function isAjax()
    {
        $oRequest = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

        return $oRequest->isAjaxRequest();
    }

    /**
     * Return list root sections for current user.
     *
     * @return array
     */
    private function getRootSections()
    {
        $result = [];

        if (!$this->hasError()) {
            global $USER;

            $rsUserField = CUser::GetList(
                $by = 'ID',
                $order = 'ASC',
                ['ID' => $USER->GetID()],
                [
                    'SELECT' => [
                        $this->arParams['USER_FIELD']
                    ]
                ]
            );
            $arUserField = $rsUserField->GetNext();

            if (count($arUserField[$this->arParams['USER_FIELD']]) > 0) {
                $oIBSections = \CIBlockSection::GetList(
                    [],
                    [
                        'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
                        $this->arParams['SECTION_FIELD'] => $arUserField[$this->arParams['USER_FIELD']],
                        'ACTIVE' => 'Y',
                    ],
                    false,
                    ['ID']
                );
                while ($arSection = $oIBSections->GetNext()) {
                    $result[] = (int)$arSection['ID'];
                }
            } else {
                return $result;
            }
        }

        return $result;
    }
}
