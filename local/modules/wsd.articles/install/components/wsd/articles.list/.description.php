<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) {
    die();
}

use Bitrix\Main\Localization\Loc;

$arComponentDescription = [
	'NAME' => Loc::getMessage('WSD_ARL_IBLOCK_NAME'),
	'DESCRIPTION' => Loc::getMessage('WSD_ARL_IBLOCK_DESCRIPTION'),
	'ICON' => '/images/articles_list.gif',
	'COMPLEX' => 'N',
	'PATH' => [
		'ID' => 'service',
		'CHILD' => [
			'ID' => 'articles',
			'NAME' => Loc::getMessage('WSD_ARL_IBLOCK_TYPE'),
			'SORT' => 15,
			'CHILD' => [
				'ID' => 'articles_complex',
			],
		],
	],
];
