<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

/**
 * @var $arCurrentValues array
 */

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

try {
    if (!Loader::includeModule('iblock')) {
        ShowError(Loc::getMessage('WSD_ASL_E_MODULE_IBLOCK'));
        return;
    }

    if (!Loader::includeModule('wsd.articles')) {
        ShowError(Loc::getMessage('WSD_ASL_E_MODULE_WSD_ARTICLES'));
        return;
    }
} catch (\Exception $e) {
    ShowError($e->getMessage());
    return;
}

// список типов инфоблоков
$arIBlockType = CIBlockParameters::GetIBlockTypes();

// список инфоблоков
$arIBlock = [];
$rsIBlock = CIBlock::GetList(
    ['SORT' => 'ASC'],
    [
        'TYPE' => $arCurrentValues['IBLOCK_TYPE'],
        'ACTIVE' => 'Y',
    ]
);
while ($arr = $rsIBlock->Fetch()) {
    $arIBlock[$arr['ID']] = sprintf('[%s] %s', $arr['ID'], $arr['NAME']);
}

// направления сортировки
$arSorts = [
    'ASC' => Loc::getMessage('WSD_ASL_SORT_ASC'),
    'DESC' => Loc::getMessage('WSD_ASL_SORT_DESC'),
];

// поля для сортировки
$arSortFields = [
    'ID' => 'ID',
    'NAME' => Loc::getMessage('WSD_ASL_SORT_NAME'),
    'ACTIVE_FROM' => Loc::getMessage('WSD_ASL_SORT_ACTIVE_FROM'),
    'SORT' => Loc::getMessage('WSD_ASL_SORT_SORT'),
    'TIMESTAMP_X' => Loc::getMessage('WSD_ASL_SORT_TIMESTAMP_X'),
];

// список пользовательских полей пользователя
$arUUFs = [];
$rsUf = CUserTypeEntity::GetList(
    ['NAME' => 'ASC'],
    ['ENTITY_ID' => 'USER', 'LANG' => LANGUAGE_ID]
);
while ($arUF = $rsUf->GetNext()) {
    $arUUFs[$arUF['FIELD_NAME']] = sprintf('[%s] %s', $arUF['FIELD_NAME'], $arUF['EDIT_FORM_LABEL']);
}

// список пользовательских полей раздела
$arSUFs = [];
if ($arCurrentValues['IBLOCK_ID'] > 0) {
    $rsUf = CUserTypeEntity::GetList(
        ['NAME' => 'ASC'],
        ['ENTITY_ID' => 'IBLOCK_' . (int)$arCurrentValues['IBLOCK_ID'] . '_SECTION', 'LANG' => LANGUAGE_ID]
    );
    while ($arUF = $rsUf->GetNext()) {
        $arSUFs[$arUF['FIELD_NAME']] = sprintf('[%s] %s', $arUF['FIELD_NAME'], $arUF['EDIT_FORM_LABEL']);
    }
}

$arComponentParameters = [
    'GROUPS' => [
        'SECTION' => [
            'NAME' => Loc::getMessage('WSD_ASL_GROUPS_SECTION'),
            'SORT' => 580,
        ],
        'ELEMENT' => [
            'NAME' => Loc::getMessage('WSD_ASL_GROUPS_ELEMENT'),
            'SORT' => 590,
        ],
        'QUERY' => [
            'NAME' => Loc::getMessage('WSD_ASL_GROUPS_QUERY'),
            'SORT' => 600,
        ]
    ],
    'PARAMETERS' => [
        'IBLOCK_TYPE' => [
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('WSD_ASL_IBLOCK_TYPE'),
            'TYPE' => 'LIST',
            'VALUES' => $arIBlockType,
            'REFRESH' => 'Y',
        ],
        'IBLOCK_ID' => [
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('WSD_ASL_IBLOCK'),
            'TYPE' => 'LIST',
            'VALUES' => $arIBlock,
            'REFRESH' => 'Y',
            'ADDITIONAL_VALUES' => 'Y',
        ],
        'ALIAS_ROOT_SECTION' => [
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('WSD_ASL_ALIAS_ROOT_SECTION'),
            'TYPE' => 'STRING',
            'DEFAULT' => Loc::getMessage('WSD_ASL_ALIAS_ROOT_SECTION_DEFAULT'),
        ],
        'USER_FIELD' => [
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('WSD_ASL_USER_FIELD'),
            'TYPE' => 'LIST',
            'REFRESH' => 'Y',
            'VALUES' => $arUUFs,
        ],
        'SECTION_FIELD' => [
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('WSD_ASL_SECTION_FIELD'),
            'TYPE' => 'LIST',
            'VALUES' => $arSUFs,
        ],
        /*'SORT_BY1' => [
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('WSD_ASL_SORT_BY1'),
            'TYPE' => 'LIST',
            'DEFAULT' => 'ACTIVE_FROM',
            'VALUES' => $arSortFields,
            'ADDITIONAL_VALUES' => 'Y',
        ],
        'SORT_ORDER1' => [
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('WSD_ASL_SORT_ORDER1'),
            'TYPE' => 'LIST',
            'DEFAULT' => 'DESC',
            'VALUES' => $arSorts,
            'ADDITIONAL_VALUES' => 'N',
        ],
        'SORT_BY2' => [
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('WSD_ASL_SORT_BY2'),
            'TYPE' => 'LIST',
            'DEFAULT' => 'SORT',
            'VALUES' => $arSortFields,
            'ADDITIONAL_VALUES' => 'Y',
        ],
        'SORT_ORDER2' => [
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('WSD_ASL_SORT_ORDER2'),
            'TYPE' => 'LIST',
            'DEFAULT' => 'ASC',
            'VALUES' => $arSorts,
            'ADDITIONAL_VALUES' => 'N',
        ],*/
        'QUERY_ASYNC' => [
            'PARENT' => 'QUERY',
            'NAME' => Loc::getMessage('WSD_ASL_QUERY_ASYNC'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'Y',
        ],
    ],
];

if ((int)$arCurrentValues['IBLOCK_ID']) {

    $typesJS = [];
    /** @var \WSD\Articles\Fields\Type[] $arTypes */
    $arTypes = \WSD\Articles\Fields\Types::getList();
    foreach ($arTypes as $type) {
        $typesJS[] = [
            'name' => $type->GetName(),
            'code' => $type->GetCode(),
        ];
    }
    unset($arTypes, $type);

    $fieldSecJS = [];
    /** @var \WSD\Articles\Fields\Field[] $arSecFields */
    $arSecFields = \WSD\Articles\Fields\Fields::getList(new CUserTypeEntity, (int)$arCurrentValues['IBLOCK_ID']);
    foreach ($arSecFields as $secField) {
        $fieldSecJS[] = [
            'name' => $secField->GetName(),
            'code' => $secField->GetCode(),
        ];
    }
    unset($arSecFields, $secField);

    $fieldElJS = [];
    /** @var \WSD\Articles\Fields\Field[] $arElFields */
    $arElFields = \WSD\Articles\Fields\Fields::getList(new CIBlockProperty, (int)$arCurrentValues['IBLOCK_ID']);
    foreach ($arElFields as $elField) {
        $fieldElJS[] = [
            'name' => $elField->GetName(),
            'code' => $elField->GetCode(),
        ];
    }
    unset($arElFields, $elField);

    $arComponentParameters['PARAMETERS']['ELEMENT_FIELDS'] = [
        'NAME' => Loc::getMessage('WSD_ASL_SHOW_FIELDS'),
        'TYPE' => 'CUSTOM',
        'JS_FILE' => getLocalPath('modules/wsd.articles/install/components/wsd/articles.list/parameters/js/settings.js'),
        'JS_EVENT' => 'OnSettingsEdit',
        'JS_DATA' => json_encode([
            'LANG' => [],
            'PREFIX' => 'element',
            'TYPES' => $typesJS,
            'FIELDS' => $fieldElJS,
        ]),
        'DEFAULT' => null,
        'PARENT' => 'ELEMENT',
    ];

    $arComponentParameters['PARAMETERS']['SECTION_FIELDS'] = [
        'NAME' => Loc::getMessage('WSD_ASL_SHOW_FIELDS'),
        'TYPE' => 'CUSTOM',
        'JS_FILE' => getLocalPath('modules/wsd.articles/install/components/wsd/articles.list/parameters/js/settings.js'),
        'JS_EVENT' => 'OnSettingsEdit',
        'JS_DATA' => json_encode([
            'LANG' => [],
            'PREFIX' => 'section',
            'TYPES' => $typesJS,
            'FIELDS' => $fieldSecJS,
        ]),
        'DEFAULT' => null,
        'PARENT' => 'SECTION',
    ];
}
