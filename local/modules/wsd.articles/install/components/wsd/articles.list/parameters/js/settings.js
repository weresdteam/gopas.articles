function OnSettingsEdit(arParams) {
    let resultInput = arParams.oInput,
        jsOptions = JSON.parse(arParams.data),
        prefix = jsOptions.PREFIX,
        dataInput = [],
        defaultDataInput = [{
            code: 'NAME',
            type: 'STRING',
            sort: '100',
            require: false,
            multiple: false,
        }];

    try {
        dataInput = JSON.parse(arParams.oInput.value);
    } catch (e) {
        dataInput = Object.assign([], defaultDataInput);
        console.log('Error parse "' + prefix + '" setting JSON, set default value!');
    }


    if (!wsd__isValidSetting(dataInput)) {
        dataInput = Object.assign([], defaultDataInput);
    }

    let oLabel = arParams.oCont.appendChild(
        BX.create(
            'a', {
                attrs: {
                    id: 'button-option-list-' + prefix,
                    href: '#'
                },
                html: 'Изменить'
            }
        )
    );

    let addAnswer = new BX.PopupWindow('popupSetting_' + prefix, null, {
        content: BX.create('span', {
            attrs: {
                id: 'option-list-' + prefix
            }
        }),
        closeIcon: {
            right: '20px',
            top: '10px'
        },
        titleBar: {
            content: BX.create('H5', {
                html: 'Поля ' + (prefix == 'element' ? 'элементов' : 'разделов'),
                props: {
                    className: 'access-title-bar'
                },
                attrs: {
                    style: 'margin-top: 15px'
                }
            })
        },
        zIndex: 5500,
        offsetLeft: 0,
        offsetTop: 0,
        draggable: {
            restrict: false
        },
        overlay: {
            opacity: 75
        },
        buttons: [
            new BX.PopupWindowButton({
                text: 'Сохранить',
                className: 'popup-window-button-accept',
                events: {
                    click: function () {
                        resultInput.value = cmpOptions.formData;
                        this.popupWindow.close();
                    }
                }
            }),
            new BX.PopupWindowButton({
                text: 'Закрыть',
                className: 'webform-button-link-cancel',
                events: {
                    click: function () {
                        this.popupWindow.close();
                    }
                }
            })
        ]
    });

    BX.bindDelegate(
        arParams.oCont,
        'click',
        { id: 'button-option-list-' + prefix },
        function () { 
            addAnswer.show();
            event.preventDefault();
        }
    );

    let cmpOptions = new Vue({
        el: '#option-list-' + prefix,
        data: {
            fields: jsOptions.FIELDS,
            types: jsOptions.TYPES,
            store: dataInput
        },
        methods: {
            addStoreItem: function() {
                let item = Object.assign({}, this.store.slice(-1).shift());
                this.store.push(item);
                event.preventDefault();
            },
            deleteStoreItem: function(index) {
                this.store = this.store.filter((v, i) => { return i !== index; });
                event.preventDefault();
            }
        },
        computed: {
            formData: function() {
                return JSON.stringify(this.store);
            }
        },
        template: `
            <form name="">
                <table style="text-align: center">
                    <tr>
                        <th>Имя поля</th>
                        <th>Тип поля</th>
                        <th>Сортировка</th>
                        <th>Обязательное</th>
                        <th>Множественное</th>
                        <th></th>
                    </tr>
                    <tr v-for="(item, index) in store">
                        <td>
                            <select name="FIELD_NAME[]" v-model="item.code">
                                <option v-for="op in fields" v-bind:value="op.code">{{ op.name }}</option>
                            </select>
                        </td>
                        <td>
                            <select name="FIELD_TYPE[]" v-model="item.type">
                                <option v-for="tp in types" v-bind:value="tp.code">{{ tp.name }}</option>
                            </select>
                        </td>
                        <td>
                            <input type="text" name="FIELD_SORT[]" v-model="item.sort" size="6">
                        </td>
                        <td>
                            <input type="checkbox" name="FIELD_REQ[]" v-model="item.require">
                        </td>
                        <td>
                            <input type="checkbox" name="FIELD_MULTIPLE[]" v-model="item.multiple">
                        </td>
                        <td>
                            <a href="#" @click="deleteStoreItem(index)" v-if="store.length > 1">Удалить</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="#" @click="addStoreItem">Добавить</a>
                        </td>
                        <td colspan="4"></td>
                    </tr>
                </table>
            </form>`
    });
}

function wsd__isValidSetting(obj)
{
    if (typeof obj !== 'object') {
        return false;
    }

    try {
        for (let id in obj) {
            let item = obj[id];

            if (item) {
                if (
                    !(typeof item['code'] === 'string') ||
                    !(typeof item['type'] === 'string') ||
                    !(typeof item['sort'] === 'string') ||
                    !(typeof item['require'] === 'boolean') ||
                    !(typeof item['multiple'] === 'boolean')
                ) {
                    return false;
                }
            } else {
                return false;
            }
        }
    } catch (e) {
        return false;
    }

    return true;
}