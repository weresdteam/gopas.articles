<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

$MESS['WSD_ASL_E_MODULE_IBLOCK'] = 'Ошибка подключения модуля iblock.';
$MESS['WSD_ASL_E_IBLOCK_ID'] = 'Не указан идентификатор инфоблока.';
$MESS['WSD_ASL_E_ROOT_SECTIONS'] = 'Не привязаны сайты к разделам или пользователям.';
$MESS['WSD_ASL_E_USER_FIELD'] = 'Не указано свойство для пользователя.';
$MESS['WSD_ASL_E_SECTION_FIELD'] = 'Не указано свойство для раздела.';
