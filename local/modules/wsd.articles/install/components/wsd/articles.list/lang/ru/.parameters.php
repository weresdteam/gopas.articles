<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

/** ошибки */
$MESS['WSD_ASL_E_MODULE_IBLOCK'] = 'Не подключен модуль iblock';
$MESS['WSD_ASL_E_MODULE_WSD_ARTICLES'] = 'Не подключен модуль wsd.articles';

/** сортировка */
$MESS['WSD_ASL_SORT_ASC'] = 'По возрастанию';
$MESS['WSD_ASL_SORT_DESC'] = 'По убыванию';
$MESS['WSD_ASL_SORT_NAME'] = 'Название';
$MESS['WSD_ASL_SORT_ACTIVE_FROM'] = 'Дата начала активности';
$MESS['WSD_ASL_SORT_SORT'] = 'Сортировка';
$MESS['WSD_ASL_SORT_TIMESTAMP_X'] = 'Дата последнего изменения';
$MESS['WSD_ASL_ROOT_SECTION'] = 'Идентифифкатор раздела';
$MESS['WSD_ASL_SORT_BY1'] = 'Направление первой сортировки';
$MESS['WSD_ASL_SORT_ORDER1'] = 'Поле для первой сортировки';
$MESS['WSD_ASL_SORT_BY2'] = 'Направление второй сортировки';
$MESS['WSD_ASL_SORT_ORDER2'] = 'Поле для второй сортировки';

/** группы */
$MESS['WSD_ASL_GROUPS_QUERY'] = 'Параметры запросов';
$MESS['WSD_ASL_GROUPS_ELEMENT'] = 'Параметры элемента';
$MESS['WSD_ASL_GROUPS_SECTION'] = 'Параметры раздела';

/** остальное */
$MESS['WSD_ASL_IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['WSD_ASL_IBLOCK'] = 'Инфоблок';
$MESS['WSD_ASL_USER_FIELD'] = 'Поле пользователя в котором хранится привязка к разделу';
$MESS['WSD_ASL_SECTION_FIELD'] = 'Поле раздела в котором хранится привязка к разделу';
$MESS['WSD_ASL_ELEMENT_FIELDS'] = 'Редактируемые поля елемента';
$MESS['WSD_ASL_SECTION_FIELDS'] = 'Редактируемые поля раздела';
$MESS['WSD_ASL_QUERY_ASYNC'] = 'Асинхронные запроссы';
$MESS['WSD_ASL_ALIAS_ROOT_SECTION'] = 'Алиас корневых разделов';
$MESS['WSD_ASL_ALIAS_ROOT_SECTION_DEFAULT'] = 'Корень';
$MESS['WSD_ASL_SHOW_FIELDS'] = 'Поля отображаемые при редактировании';

