<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

$MESS['WSD_ARL_IBLOCK_NAME'] = 'Список статей';
$MESS['WSD_ARL_IBLOCK_DESCRIPTION'] = 'Работа со структкрой на основе статей.';
$MESS['WSD_ARL_IBLOCK_TYPE'] = 'Структура из статей';
