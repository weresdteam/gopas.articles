<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arParams */
/** @var array $arResult */

$this->setFrameMode(true);

if (count($arResult['ERRORS']) > 0) {
    ShowError(implode(', ', $arResult['ERRORS']));
} else { ?>
    <div>
        <div id="article-list">
            <asl-message></asl-message>
            <router-view></router-view>
            <asl-overload></asl-overload>
        </div>
        <script>
            window.aslVueConfig = Object.freeze({
                query: {
                    type: 'GET',
                    data: {},
                    async: <?= $arParams['QUERY_ASYNC'] ? 'true' : 'false' ?>,
                    dataType: 'json',
                    url: '<?= $APPLICATION->GetCurPage() ?>',
                    headers: {
                        'X-Bitrix-Csrf-Token': '<?= bitrix_sessid() ?>',
                    },
                },
                messageTime: 15,
                root: <?= json_encode($arResult['ROOT_SECTIONS'] ?: []) ?>
            });
            if ($.trumbowyg) {
                $.trumbowyg.svgPath = '<?= getLocalPath('modules/wsd.articles/assets/js/trumbowyg/ui/icons.svg') ?>';
            }
        </script>
    </div>
<?php } ?>

