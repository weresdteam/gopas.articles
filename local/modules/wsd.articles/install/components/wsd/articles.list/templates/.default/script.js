/**
 * Компонент asl-section.
 * Отвечает за рекурсивную отрисовку разделов.
 */
Vue.component(
    'asl-section',
    {
        props: {
            section: Object
        },
        methods: {
            getSectionInfo: function (event) {
                event.preventDefault();
                this.$store.dispatch('getInfo', this.section.id);
            },
            itemAction: function (action) {
                if (action === 'delete') {
                    this.$store.dispatch(
                        'delSection',
                        {
                            id: this.section.id,
                            section_id: this.section.parent
                        }
                    );
                }

                if (action === 'edit') {
                    this.$router.push({
                        name: 'section',
                        params: {
                            section_id: this.section.parent,
                            id: this.section.id,
                        }
                    });
                }

                if (action === 'add_sections') {
                    this.$router.push({
                        name: 'section',
                        params: {
                            section_id: this.section.id,
                            id: 0,
                        }
                    });
                }

                if (action === 'add_elements') {
                    this.$router.push({
                        name: 'element',
                        params: {
                            section_id: this.section.id,
                            id: 0,
                        }
                    });
                }
            }
        },
        computed: {
            isRootSection: function() {
                return this.section.parent === 0;
            }
        },
        template: `
            <div 
                v-if="Object.keys(section).length > 0"
                v-show="!section.state.hide" 
                v-bind:class="{ offset : section.state.padding }"
            >
                <h3 v-if="isRootSection" class="article-list__item">
                    {{ section.name }}&nbsp;
                    <asl-item-menu v-bind:flags="section.actions" @change-item="itemAction"></asl-item-menu>
                </h3>
                <h4 v-else class="article-list__item">
                    <i class="far fa-folder"></i>
                    <a href="#" class="item-title" @click="getSectionInfo">{{ section.name }}</a>
                    <asl-item-menu v-bind:flags="section.actions" @change-item="itemAction"></asl-item-menu>
                </h4>
                <asl-element
                    v-if="section.elements.length > 0"
                    v-for="id in section.elements"
                    v-bind:key="'e_' + id"
                    v-bind:element="$store.state.elements[id] || {}">
                </asl-element>
                <asl-section
                    v-if="section.sections.length > 0"
                    v-for="id in section.sections"
                    v-bind:key="'s_' + id"
                    v-bind:section="$store.state.sections[id] || {}">
                </asl-section>
            </div>
        `
    }
);

/**
 * Компонент asl-element.
 * Отвечает за отрисовку елементов.
 */
Vue.component(
    'asl-element',
    {
        props: {
            element: Object
        },
        methods: {
            itemAction: function (action) {
                if (action === 'delete') {
                    this.$store.dispatch(
                        'delElement',
                        {
                            id: this.element.id,
                            section_id: this.element.parent
                        }
                    );
                }

                if (action === 'edit') {
                    this.$router.push({
                        name: 'element',
                        params: {
                            section_id: this.element.parent,
                            id: this.element.id,
                        }
                    });
                }
            }
        },
        template: `
            <div 
                v-if="Object.keys(element).length > 0"
                v-show="!element.state.hide" 
                v-bind:class="{ offset : element.state.padding }"
                class="article-list__item"
            >
                <i class="far fa-file"></i>{{ element.name }}
                <asl-item-menu v-bind:flags="element.actions" @change-item="itemAction"></asl-item-menu>
            </div>
        `,
    }
);

/**
 * Компонент asl-item-menu.
 * Отвечает за отрисовку меню для разделов и элементов.
 */
Vue.component(
    'asl-item-menu',
    {
        props: {
            flags: Object
        },
        data: function () {
            return {
                isShowConfirm: false,
                icons: {
                    add_elements: 'fa-file-medical',
                    add_sections: 'fa-folder-plus',
                    edit: 'fa-pen',
                    delete: 'fa-trash-alt',
                },
            };
        },
        methods: {
            changeItem: function (key) {
                // нужно подтверждение и режим подтверждения выключен
                if (key === 'delete' && this.isShowConfirm !== true) {
                    this.isShowConfirm = true;
                } else {
                    this.isShowConfirm = false;
                    this.$emit('change-item', key);
                }
            },
        },
        template: `
            <span v-if="Object.keys(flags).length > 0" class="asl-item-menu">
                <span 
                    v-for="(value, key) in flags"
                    v-if="value" 
                    v-bind:key="key"
                    class="asl-item-menu__btn"
                >
                    <span
                            class="asl-small-btn" 
                            v-show="!isShowConfirm"
                            @click="changeItem(key)">
                        <i 
                            class="fas" 
                            v-bind:class="icons[key] || 'fa-info-circle'"
                        >
                        </i>
                    </span>
                    <asl-small-confirm
                        v-if="key === 'delete'" 
                        v-show="isShowConfirm"
                        @confirm-agree="changeItem(key)"
                        @confirm-cancel="isShowConfirm = false"
                    >
                    </asl-small-confirm>
                </span>
            </span>
        `
    }
);

/**
 * Компонент asl-small-confirm.
 * Отвечает за отрисовку и функционал подтверждения действия.
 */
Vue.component(
    'asl-small-confirm',
    {
        template: `
            <span class="asl-small-confirm">
                <i class="fas fa-check btn-agree asl-small-btn" @click="agree"></i>
                <i class="fas fa-ban btn-cancel asl-small-btn"  @click="cancel"></i>
            </span>    
        `,
        methods: {
            agree: function () {
                this.$emit('confirm-agree');
            },
            cancel: function () {
                this.$emit('confirm-cancel');
            },
        }
    }
);

Vue.component(
    'asl-nav-bar',
    {
        methods: {
            back: function(event) {
                event.preventDefault();
                this.$router.push({ name: 'tree', params: { id: window.aslVueConfig.root } });
            }
        },
        template: `
            <div class="asl-nav-panel">
                <a @click="back" class="asl-nav-panel__back">
                     <i class="fas fa-arrow-left"></i>
                     Назад
                 </a>
            </div>
        `
    }
);

/**
 * Компонент asl-root-tree.
 * Корневой компонент для отрисовкии дерева разделов и элементов.
 */
const aslRootTree = Vue.component(
    'asl-root-tree',
    {
        template: `
        <div>
            <asl-section 
                v-for="(value, key) in this.$store.state.root"
                v-bind:key="'rs_' + value"
                v-if="$store.state.sections[value]" 
                v-bind:section="$store.state.sections[value]"
            ></asl-section>
        </div>`
    }
);

/**
 * Компонент asl-element-edit.
 * Компонент редактирования элементов.
 */
const AslElementEdit = Vue.component(
    'asl-element-edit',
    {
        data: function() {
            return {
                section_id: 0,
                id: 0,
                fields: [{
                    name: '',
                    code: '',
                    sort: 500,
                }]
            }
        },
        computed: {
            isSection: function() {
                return this.$route.name === 'section';
            },
            isValidForm: function () {
                let result = true;

                for (let key in this.fields) {
                    let field = this.fields[key];

                    if (field.require) {
                        let value = field.value;

                        if (typeof field.value == 'object') {
                            let isOk = false;

                            for (let val of field.value) {
                                if (val.hasOwnProperty('type') && val.type === 'file') {
                                    if (val.status >= 0 && !isOk) isOk = true;
                                }
                            }

                            if (!isOk) result = false;
                        }

                        if (String(value).length === 0) {
                            result = false;
                        }
                    }
                }

                return result;
            }
        },
        created: function() {
            let self = this;
            self.section_id = self.$route.params.section_id;
            self.id = self.$route.params.id;

            if (self.$route.params.id >= 0) {
                ApiQuery
                    .reset()
                    .setType('GET')
                    .setData({
                        section_id: self.section_id,
                        id: self.id,
                        action: this.isSection ? 2 : 3,
                    })
                    .setSuccess(function (answer) {
                        if (answer.error) {
                            self.$store.commit('setMessage', answer.msg);
                        } else {
                            Vue.set(self, 'fields', Object.assign([], answer.fields));
                        }
                    })
                    .query();
            }
        },
        methods: {
            save: function() {
                let fd = new FormData;

                fd.append('id', this.id);
                fd.append('section_id', this.section_id);

                for (let field of this.fields) {
                    switch (typeof field.value){
                        case 'object':
                            for (let val of field.value) {
                                if (val.hasOwnProperty('type') && val.type === 'file') {
                                    if (val.status == 0) {
                                        fd.append('FILE_' + val.id, val.data);
                                    }
                                }
                            }

                            fd.append(field.code, JSON.stringify(field.value));
                            break;
                        default:
                            if (field.hasOwnProperty('text_type')) {
                                fd.append(field.code + '_TYPE', field.text_type);
                            }

                            fd.append(field.code, field.value);
                            break
                    }
                }

                if (this.isSection) {
                    this.$store.dispatch('saveSection', fd);
                } else {
                    this.$store.dispatch('saveElement', fd);
                }
            }
        },
        template: `
            <div class="asl-edit">
                <asl-nav-bar></asl-nav-bar>
                <h3>{{ id > 0 ? 'Редактирование' : 'Добавление'}}&nbsp;{{ isSection ? 'раздела' : 'элемента' }}</h3>
                <h4 v-if="section_id > 0">Родительский раздел:&nbsp;{{ this.$store.state.sections[section_id].name }}</h4>
                <asl-form-fields v-bind:fields="fields"></asl-form-fields>
                <hr>
                <div class="asl-edit__row">
                    <a 
                        v-if="isValidForm"
                        href="#" 
                        class="wsd-btn" 
                        @click="save"
                    ><i class="far fa-save"></i>Сохранить
                    </a>
                    <label v-else><b>Сохранение не возможно, не запонены обязательные поля</b></label>
                </div>
            </div>
        `
    }
);

/**
 * Компонент asl-overload.
 * Компонент с оверлоадом.
 */
Vue.component(
    'asl-overload',
    {
        template: `<div v-show="$store.state.isLoading" class="article-list__loading">
            <div class="article-list__loading-icon"></div>
        </div>`
    }
);

/**
 * Компонент asl-message.
 * Компонент отображения сообщений.
 */
Vue.component(
    'asl-message',
    {
        created: function() {
            this.$store.commit('runMsgController');
        },
        template: `<div 
            class="article-list__message" 
        >
            <div v-for="msg in $store.state.msg">
                <transition name="fade">
                    <p v-if="msg.isShow" v-bind:class="{ error : msg.type == 'error' }">
                        {{ msg.text }}
                        <i class="fas fa-times" @click="msg.isShow = false"></i>
                    </p>
                 </transition>
             </div>
        </div>`
    }
);

Vue.component(
    'asl-form-fields',
    {
        props: {
            fields: Array
        },
        template: `
            <div class="asl-edit__row">
                <div 
                    v-for="(field, i) in fields" 
                    v-bind:key="i"
                >
                
                    <template v-if="field.type == 'STRING'">
                        <asl-form-field-string v-bind:field="field"></asl-form-field-string>
                    </template>
                    
                    <template v-if="field.type == 'NUMBER'">
                        <asl-form-field-string v-bind:field="field"></asl-form-field-string>
                    </template>
                    
                    <template v-if="field.type == 'TEXT'">
                        <asl-form-field-text v-bind:field="field"></asl-form-field-text>
                    </template>                    
                    
                    <template v-if="field.type == 'IMAGE'">
                        <asl-form-field-file v-bind:field="field" v-bind:isImages="true"></asl-form-field-file>
                    </template>
                    
                    <template v-if="field.type == 'FILE'">
                        <asl-form-field-file v-bind:field="field" v-bind:isImages="false"></asl-form-field-file>
                    </template>
                    
                </div>
            </div>
        `
    }
);

Vue.component(
    'asl-form-field-title',
    {
        props: {
            name: String,
            require: Boolean
        },
        template: `
            <label>
                <template v-if="require"><b>{{ name }}:*</b></template>
                <template v-else>{{ name }}:</template>
            </label>
        `
    }
);

Vue.component(
    'asl-form-field-string',
    {
        props: {
            field: Object
        },
        template: `
            <div>
                <asl-form-field-title :name="field.name" :require="field.require"></asl-form-field-title>
                <input type="text" v-model="field.value">
            </div>
        `
    }
);

Vue.component(
    'asl-form-field-text',
    {
        props: {
            field: Object
        },
        mounted: function() {
            if (!this.field.text_type) {
                Vue.set(this.field, 'text_type', 'text');
            }

            let self = this;

            $('.' + this.uniqKey + ' .html_editor')
                .trumbowyg('')
                .on('tbwchange', function(){
                    self.field.value = $(this).val();
                });
        },
        watch: {
            'field.value': function() {
                $('.' + this.uniqKey + ' .html_editor').trumbowyg('html', this.field.value);
            },
        },
        computed: {
            uniqKey: function() {
                return 'html_text_' + this.field.code;
            },
            isSimpleField: function() {
                let codes = [
                    'PREVIEW_TEXT',
                    'DETAIL_TEXT',
                    'DESCRIPTION'
                ];

                return codes.indexOf(this.field.code) === -1;
            }
        },
        template: `
            <div v-bind:class="uniqKey">
                <asl-form-field-title :name="field.name" :require="field.require"></asl-form-field-title>

                <template v-if="!isSimpleField">
                    <div v-show="field.text_type != 'html'">
                        <textarea type="text" v-model="field.value" rows="10"></textarea>
                    </div>
                    <div  v-show="field.text_type == 'html'">
                        <textarea class="html_editor"  v-model="field.value" rows="10"></textarea>
                    </div>
                </template>
                
                <template v-else>
                    <div>
                        <textarea v-model="field.value" rows="10"></textarea>
                    </div>
                </template>

                <div v-if="!isSimpleField">
                    <label>Тип поля "{{ field.name }}":</label>
                    <label>
                        <input type="radio" v-model="field.text_type" value="text">
                        <span>Текст</span>
                    </label>
                    <label>
                        <input type="radio" v-model="field.text_type" value="html">
                        <span>HTML</span>
                    </label>
                </div>
                
            </div>
        `
    }
);

Vue.component(
    'asl-form-field-file',
    {
        props: {
            field: Object,
            isImages: Boolean
        },
        data: function () {
            return {
                imgStore: {}
            }
        },
        methods: {
            addFile: function() {
                if (!event.target.files.length) {
                    return;
                } else {
                    if (this.field.multiple === false) Vue.set(this.field, 'value', []);

                    for (let oFile of event.target.files) {
                        this.field.value.push({
                            id: Math.random().toString().replace('.',''),
                            name: oFile.name,
                            description: '',
                            path: '',
                            status: 0,
                            data: oFile,
                            type: 'file'
                        });
                    }

                    event.target.value = '';
                }
            },
            delFile: function (id) {
                let clear = false;

                for (let val of this.field.value) {
                    if (val.id == id) {
                        if (val.status == 0) clear = true;
                        if (val.status == 1) val.status = -1;
                    }
                }

                if (clear) Vue.set(this.field, 'value', this.field.value.filter(el => el.id != id));
            },
            getImg: function(oFile) {
                if (oFile.path.length > 0) {
                    return oFile.path;
                }

                let id = String(oFile.id);

                if (this.imgStore.hasOwnProperty(id)) {
                    return this.imgStore[id];
                } else {
                    Vue.set(this.imgStore, id, '');
                }

                if (oFile.data instanceof File) {
                    let reader = new FileReader;
                    reader.onload = (e) => { Vue.set(this.imgStore, id, e.target.result); };
                    reader.readAsDataURL(oFile.data);
                }

                return false;
            }
        },
        template: `
            <div>
                <asl-form-field-title :name="field.name" :require="field.require"></asl-form-field-title>
                
                <div v-for="(val, i) in field.value" v-bind:key="i" v-if="val.status >=0">
                    <img v-if="isImages" :src="getImg(val)" :alt="val.name" class="asl-edit__img">
                    <br>
                    <input type="text" placeholder="Описание" v-model="val.description">
                    <br>
                    <span>{{ val.name }}&nbsp;</span>
                    <span class="wsd-btn" @click="delFile(val.id)" title="Удалить">x</span>
                </div>
                
                <div v-if="field.multiple || (!field.multiple && field.value.length == 0 || (field.value.length == 1 && field.value[0].status == -1))">
                    <label class="wsd-btn">
                        <span>Добавить {{ isImages ? 'изображение' : 'Файл' }}</span>
                        <input type="file" @change="addFile">
                    </label>
                </div>
            </div>
        `
    }
);

$(document).ready(function () {
    if (window.aslVueConfig) {
        window.ApiQuery = {
            params: Object.assign({}, window.aslVueConfig.query),
            success: function (answer) {
                console.log(answer);
            },
            error: function (answer) {
                console.log(answer);
            },
            setType: function (type) {
                this.params.type = type;
                return this;
            },
            setData: function (data) {
                this.params.data = data;
                return this;
            },
            setAsync: function (async) {
                this.params.async = async;
                return this;
            },
            setDataType: function (type) {
                this.params.dataType = type;
                return this;
            },
            setSuccess: function (success) {
                this.success = success;
                return this;
            },
            addHeads: function (heads) {
                this.params.headers = Object.assign(this.params.headers, heads);
                return this;
            },
            setProcessData: function(data) {
                this.params.processData = data;
                return this;
            },
            setContentType: function (type) {
                this.params.contentType = type;
                return this;
            },
            query: function () {
                window.vueStore.commit('setLoading', true);

                if (this.params.type === 'DELETE') {
                    let separator = '?';
                    if (this.params.url.match(/\?/) !== null) {
                            separator = '&';
                    }
                    this.params.url += separator + $.param(this.params.data);
                    this.params.data = {};
                }

                $
                    .ajax(this.params)
                    .done(this.success)
                    .fail(this.error)
                    .always(function(){ window.vueStore.commit('setLoading', false) });
            },
            reset: function() {
                this.params = Object.assign({}, window.aslVueConfig.query);
                return this;
            }
        };

        window.vueStore = new Vuex.Store({
            state: {
                isLoading: false,
                msg: [],
                sections: {},
                elements: {},
                root: {},
                msgControllerId: 0,
            },
            getters: {},
            mutations: {
                setElements: (state, elements) => {
                    if (!state.elements) {
                        Vue.set(state, 'elements', {});
                    }

                    for (let id in elements) {
                        Vue.set(state.elements, id, elements[id]);
                    }
                },
                setSections: (state, sections) => {
                    if (!state.sections) {
                        Vue.set(state, 'sections', {});
                    }

                    for (let id in sections) {
                        Vue.set(state.sections, id, sections[id]);
                    }
                },
                setRootSection: (state, ids) => {
                    Vue.set(state, 'root', ids);
                },
                deleteSection: (state, id) => {
                    let parent = state.sections[id].parent;
                    let parentSections = state.sections[parent].sections;

                    Vue.set(state.sections[parent].sections, parentSections.indexOf(id), 0);

                    if (
                        state.sections[parent].elements.filter((v) => { return v > 0; }).length === 0 &&
                        state.sections[parent].sections.filter((v) => { return v > 0; }).length === 0 &&
                        state.sections[parent].parent !== 0
                    ) {
                        Vue.set(state.sections[parent].actions, 'delete', true);
                    }
                },
                deleteElement: (state, id) => {
                    let parent = state.elements[id].parent;
                    let parentElements = state.sections[parent].elements;

                    Vue.set(state.sections[parent].elements, parentElements.indexOf(id), 0);

                    if (
                        state.sections[parent].elements.filter((v) => { return v > 0; }).length === 0 &&
                        state.sections[parent].sections.filter((v) => { return v > 0; }).length === 0
                    ) {
                        Vue.set(state.sections[parent].actions, 'delete', true);
                    }
                },
                setMessage: (state, msgOp) => {
                    state.msg.push({
                        text: msgOp.msg,
                        type: msgOp.type,
                        time: window.aslVueConfig.messageTime || 15,
                        isShow: true,
                    });
                    Vue.set(state, 'msg', state.msg.filter((item) => item.isShow));
                },
                runMsgController: (state) => {
                    if (state.msgControllerId > 0) return;

                    state.msgControllerId = setInterval(() => {
                        if (state.msg.length > 0) {
                            for (let msg of state.msg) {
                                if (msg.isShow) {
                                    msg.time--;
                                }

                                if (msg.time <= 0) {
                                    msg.isShow = false;
                                }
                            }
                        }
                    }, 1000);
                },
                setItem: (state, item) => {
                    Vue.set(state, 'item', item);
                },
                setLoading: (state, flag) => {
                    state.isLoading = !!flag;
                },
                setFiles: (state, files) => {
                    for (let oFile  of files) {
                        Vue.set(state, oFile.id, oFile);
                    }
                }
            },
            actions: {
                getInfo: function (context, id) {
                    ApiQuery
                        .reset()
                        .setType('GET')
                        .setData({section_id: id, action: 1})
                        .setSuccess(function (answer) {
                            if (!answer.error) {
                                context.commit('setElements', answer.elements);
                                context.commit('setSections', answer.sections);
                            } else {
                                context.commit('setMessage', { msg: answer.msg, type: 'error' });
                            }
                        })
                        .query();
                },
                getRootData: function (context) {
                    if (typeof window.aslVueConfig !== "undefined") {
                        for (let id in window.aslVueConfig.root) {
                            context.dispatch('getInfo', window.aslVueConfig.root[id]);
                        }
                        context.commit('setRootSection', window.aslVueConfig.root);
                    } else {
                        console.log('Конфигурация приложения не найдена');
                    }
                },
                saveSection: function (context, formData) {
                    formData.append('action', 6);

                    ApiQuery
                        .reset()
                        .setType('POST')
                        .setData(formData)
                        .setProcessData(false)
                        .setContentType(false)
                        .setSuccess(function (answer) {
                            if (!answer.error) {
                                context.dispatch('getInfo', formData.get('section_id'));
                                context.commit('setMessage', { msg: 'Сохранено', type: 'message' });
                            } else {
                                context.commit('setMessage', { msg: answer.msg, type: 'error' });
                            }
                        })
                        .query();
                },
                delSection: function (context, params) {
                    ApiQuery
                        .setType('DELETE')
                        .setData(Object.assign(params, {action: 4}))
                        .setSuccess(function (answer) {
                            if (!answer.error) {
                                context.commit('deleteSection', params.id);
                            } else {
                                context.commit('setMessage', { msg: answer.msg, type: 'error' });
                            }
                        })
                        .query();
                },
                saveElement: function (context, formData) {
                    formData.append('action', 7);

                    ApiQuery
                        .reset()
                        .setType('POST')
                        .setData(formData)
                        .setProcessData(false)
                        .setContentType(false)
                        .setSuccess(function (answer) {
                            if (!answer.error) {
                                context.dispatch('getInfo', formData.get('section_id'));
                                context.commit('setMessage', { msg: 'Сохранено', type: 'message' });
                            } else {
                                context.commit('setMessage', { msg: answer.msg, type: 'error' });
                            }
                        })
                        .query();
                },
                delElement: function (context, params) {
                    ApiQuery
                        .reset()
                        .setType('DELETE')
                        .setData(Object.assign(params, {action: 5}))
                        .setSuccess(function (answer) {
                            if (!answer.error) {
                                context.commit('deleteElement', params.id);
                            } else {
                                context.commit('setMessage', { msg: answer.msg, type: 'error' });
                            }
                        })
                        .query();
                }
            },
        });

        window.aslVueRouter = new VueRouter({
            routes: [
                {
                    name: 'tree',
                    path: '/root/',
                    component: aslRootTree,
                },
                {
                    name: 'section',
                    path: '/section/:section_id/:id',
                    component: AslElementEdit, //AslSectionEdit,
                },
                {
                    name: 'element',
                    path: '/element/:section_id/:id',
                    component: AslElementEdit,
                }
            ],
        });

        Vue.use({ Vuex, VueRouter });

        window.aslVueApp = new Vue({
            el: '#article-list',
            data: {
                section: window.aslVueConfig.root,
                rootAlias: window.aslVueConfig.aliasRootSection
            },
            store: vueStore,
            router: aslVueRouter,
            created: function () {
                this.$store.dispatch('getRootData');
                this.$router.push({name: 'tree'});
            }
        });
    } else {
        console.log('Конфигурация приложения не найдена');
    }
});


