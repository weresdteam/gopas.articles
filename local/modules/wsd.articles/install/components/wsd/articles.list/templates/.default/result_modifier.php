<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

CJSCore::Init([
    'wsd_vue',
    'wsd_vuex',
    'wsd_vue_router',
    'jquery',
    'trumbowyg'
]);

$pathToFont = getLocalPath('components/wsd/articles.list/templates/.default/fonts/fontawesome/css/all.min.css');
\Bitrix\Main\Page\Asset::getInstance()->addString('<link href="' . $pathToFont . '" type="text/css" rel="stylesheet">');
