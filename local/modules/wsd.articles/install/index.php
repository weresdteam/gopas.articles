<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

use Bitrix\Main\ArgumentOutOfRangeException;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;


/**
 * Class wsd_articles.
 */
class wsd_articles extends CModule
{
    /**
     * @var string Module id.
     */
    public $MODULE_ID = 'wsd.articles';
    /**
     * @var string Module version.
     */
    public $MODULE_VERSION;
    /**
     * @var string Module version date.
     */
    public $MODULE_VERSION_DATE;
    /**
     * @var string Module name.
     */
    public $MODULE_NAME;
    /**
     * @var string Module description.
     */
    public $MODULE_DESCRIPTION;
    /**
     * @var string Server root path.
     */
    private $documentRoot;


    /**
     * wsd_articles constructor.
     */
    public function __construct()
    {
        $arModuleVersion = [];

        include __DIR__ . '/version.php';

        $this->documentRoot = realpath(__DIR__ . '/../../../..');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = Loc::getMessage('WSD_A_M_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('WSD_A_M_DESCRIPTION');
        $this->PARTNER_NAME = 'Wsd inc.';
    }

    /**
     * Install module.
     *
     * @return bool
     *
     * @throws ArgumentOutOfRangeException
     */
    public function DoInstall()
    {
        global $APPLICATION;

        $GLOBALS['MODULE_INSTALL'] = [];
        $step = (int)$_REQUEST['step'];

        if ($step <= 1) {
            $GLOBALS['MODULE_INSTALL']['CHECK'] = self::checkDependencies();
            $APPLICATION->IncludeAdminFile(Loc::getMessage('WSD_A_M_INSTALL_TITLE'), __DIR__ . '/step1.php');
        } elseif (check_bitrix_sessid() && $step === 2 && $_REQUEST['install'] === 'Y') {
            $folder = $_REQUEST['folder'] == 'local' ? 'local' : 'bitrix';

            if (CopyDirFiles(
                __DIR__ . '/components',
                $this->documentRoot . '/' . $folder . '/components',
                true,
                true
            )) {
                Option::set($this->MODULE_ID, 'folder', $folder);
                ModuleManager::registerModule($this->MODULE_ID);
            }

            $GLOBALS['MODULE_INSTALL']['STATUS'] = ModuleManager::isModuleInstalled($this->MODULE_ID);
            $APPLICATION->IncludeAdminFile(Loc::getMessage('WSD_A_M_INSTALL_TITLE'), __DIR__ . '/step2.php');
        }

        return true;
    }

    /**
     * Uninstall module.
     *
     * @return bool
     *
     * @throws ArgumentOutOfRangeException
     * @throws \Bitrix\Main\ArgumentNullException
     */
    public function DoUninstall()
    {
        global $APPLICATION;

        $GLOBALS['MODULE_INSTALL'] = [];
        $step = (int)$_REQUEST['step'];

        if ($step <= 1) {
            $GLOBALS['MODULE_INSTALL']['FOLDER'] = Option::get($this->MODULE_ID, 'folder', 'local');
            $APPLICATION->IncludeAdminFile(Loc::getMessage('WSD_A_M_UNINSTALL_TITLE'), __DIR__ . '/un_step1.php');
        } elseif (check_bitrix_sessid() && $step === 2 && $_REQUEST['uninstall'] === 'Y') {
            $folder = $_REQUEST['folder'] == 'local' ? 'local' : 'bitrix';

            DeleteDirFiles(__DIR__ . '/components', $this->documentRoot . '/' . $folder . '/components');
            ModuleManager::unRegisterModule($this->MODULE_ID);
            Option::set($this->MODULE_ID, 'folder', '');

            $GLOBALS['MODULE_INSTALL']['STATUS'] = ModuleManager::isModuleInstalled($this->MODULE_ID);
            $APPLICATION->IncludeAdminFile(Loc::getMessage('WSD_A_M_UNINSTALL_TITLE'), __DIR__ . '/un_step2.php');
        }

        return true;
    }

    /**
     * Check dependence for module.
     *
     * @return bool
     */
    private static function checkDependencies()
    {
        return ModuleManager::isModuleInstalled('monday.propsite');
    }
}
