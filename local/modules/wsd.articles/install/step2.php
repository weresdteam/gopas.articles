<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

global $MODULE_INSTALL, $APPLICATION;

use Bitrix\Main\Localization\Loc;

if ($MODULE_INSTALL['STATUS'] === true) {
    $arMessage = [
        'TYPE' => 'OK',
        'MESSAGE' => Loc::getMessage('WSD_A_M_SUCCESS_TITLE'),
        'DETAILS' => Loc::getMessage('WSD_A_M_SUCCESS_TEXT'),
        'HTML' => true,
    ];
} else {
    $arMessage = [
        'TYPE' => 'ERROR',
        'MESSAGE' => Loc::getMessage('WSD_A_M_ERROR_TITLE'),
        'DETAILS' => Loc::getMessage('WSD_A_M_ERROR_TEXT'),
        'HTML' => true,
    ];
}

echo (new CAdminMessage($arMessage))->show(); ?>
<form action="<?= $APPLICATION->GetCurPage() ?>">
	<input type="hidden" name="lang" value="<?= LANG ?>">
	<input type="submit" name="" value="<?= Loc::getMessage('WSD_A_M_BUTTON_BACK') ?>">
<form>