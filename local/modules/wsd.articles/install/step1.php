<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

global $MODULE_INSTALL, $APPLICATION;

use Bitrix\Main\Localization\Loc;

if ($MODULE_INSTALL['CHECK'] === true) { ?>
    <form action="<?= $APPLICATION->GetCurPage() ?>" name="wsd_articles" method="post">
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="lang" value="<?= LANG ?>">
        <input type="hidden" name="id" value="wsd.articles">
        <input type="hidden" name="install" value="Y">
        <input type="hidden" name="step" value="2">
        <div>
            <label for="id_folder"><?= Loc::getMessage('WSD_A_M_PLACE_TITLE') ?></label>
            <select id="id_folder" name="folder">
                <option value="local">local</option>
                <option value="bitrix">bitrix</option>
            </select>
        </div>
        <br>
        <input type="submit" name="inst" value="<?= Loc::getMessage('WSD_A_M_BUTTON_INSTALL') ?>">
    </form>
<?php } else {
    $oMessage = new CAdminMessage([
        'TYPE' => 'ERROR',
        'MESSAGE' => Loc::getMessage('WSD_A_M_ERROR_TITLE'),
        'DETAILS' => Loc::getMessage('WSD_A_M_ERROR_TEXT'),
        'HTML' => true,
    ]);
    echo $oMessage->show(); ?>
    <form action="<?= $APPLICATION->GetCurPage() ?>">
        <input type="hidden" name="lang" value="<?= LANG ?>">
        <input type="submit" name="" value="<?= Loc::getMessage('WSD_A_M_BUTTON_BACK') ?>">
    <form>
<?php }
