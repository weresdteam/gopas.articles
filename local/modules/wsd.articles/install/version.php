<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

$arModuleVersion = [
    'VERSION' => '1.0.0',
    'VERSION_DATE' => '05.10.2019',
];
