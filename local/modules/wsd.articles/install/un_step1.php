<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

global $MODULE_INSTALL, $APPLICATION;

use Bitrix\Main\Localization\Loc; ?>
<form action="<?= $APPLICATION->GetCurPage() ?>" name="wsd_articles" method="post">
    <?= bitrix_sessid_post() ?>
    <input type="hidden" name="lang" value="<?= LANG ?>">
    <input type="hidden" name="id" value="wsd.articles">
    <input type="hidden" name="uninstall" value="Y">
    <input type="hidden" name="step" value="2">
    <div>
        <label for="id_folder"><?= Loc::getMessage('WSD_A_M_FOLDER_TITLE') ?></label>
        <select id="id_folder" name="folder">
            <option value="local">local</option>
            <option value="bitrix">bitrix</option>
        </select>
        <p><?= Loc::getMessage('WSD_A_M_SAVE_TITLE') . $MODULE_INSTALL['FOLDER'] ?>.</p>
    </div>
    <br>
    <input type="submit" name="inst" value="<?= Loc::getMessage('WSD_A_M_BUTTON_UNINSTALL') ?>">
</form>