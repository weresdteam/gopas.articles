<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

// Registers module JS libs
\WSD\Articles\Entity\Assets::regJSLibs();
