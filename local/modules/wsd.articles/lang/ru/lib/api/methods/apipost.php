<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

$MESS['WSD_A_ACP_E_ELEMENT_FIELDS'] = 'В парметрах не указаны поля для элементов';
$MESS['WSD_A_ACP_E_SEC'] = 'Не передан параметр section_id';
$MESS['WSD_A_ACP_E_JSON_EL_FIELDS'] = 'В парметрах указан не валидный json с полями элементов';
$MESS['WSD_A_ACP_E_REQ_EL_FIELD'] = 'Не заполнено обязательное поле элемента - #FIELD#';
$MESS['WSD_A_ACP_E_LOAD_FILE'] = 'Не загрузился файл для поля - #FIELD#';
$MESS['WSD_A_ACP_E_SECTION_FIELDS'] = 'В парметрах не указаны поля для разделов';
$MESS['WSD_A_ACP_E_JSON_SEC_FIELDS'] = 'В парметрах указан не валидный json с полями разделов';
$MESS['WSD_A_ACP_E_REQ_SEC_FIELD'] = 'Не заполнено обязательное поле раздела - #FIELD#';
