<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

$MESS['WSD_A_ACG_E_ELEMENT_FIELDS'] = 'В парметрах не указаны поля для элементов';
$MESS['WSD_A_ACG_E_SEC'] = 'Не передан параметр section_id';
$MESS['WSD_A_ACG_E_ID'] = 'Не передан параметр id';
$MESS['WSD_A_ACG_E_JSON_EL_FIELDS'] = 'В парметрах указан не валидный json с полями элементов';
$MESS['WSD_A_ACG_E_SECTION_FIELDS'] = 'В парметрах не указаны поля для разделов';
$MESS['WSD_A_ACG_E_JSON_SEC_FIELDS'] = 'В парметрах указан не валидный json с полями разделов';
