<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

$MESS['WSD_A_AC_E_SEC_ID'] = 'Не передан id корневого раздела.';
$MESS['WSD_A_AC_E_USER_UNAUTHORIZE'] = 'Текущий пользователь не авторизован';
$MESS['WSD_A_AC_E_CSRF'] = 'Не пройдена CSRF проверка';
$MESS['WSD_A_AC_E_SEC_DENIDE'] = 'Запрет доступа к разделу section: #SEC#, user: #USER#';
$MESS['WSD_A_AC_E_WRONG_CLASS'] = 'Вызов не существующего класса API: #CLASS#';
$MESS['WSD_A_AC_E_WRONG_METHOD'] = 'Вызов не существующего метода API: #CLASS#::#METHOD#';
$MESS['WSD_A_AC_ERROR'] = 'Ошибка на стороне API, попробуйте позже';
