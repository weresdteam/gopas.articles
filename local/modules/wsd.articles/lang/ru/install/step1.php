<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

$MESS['WSD_A_M_PLACE_TITLE'] = 'Куда скопировать компоненты:';
$MESS['WSD_A_M_ERROR_TITLE'] = 'Ошибка!';
$MESS['WSD_A_M_ERROR_TEXT'] = nl2br('Одно из условий не соблюдено:' . PHP_EOL .
    '- не установлен зависимый модуль monday.propsite');
$MESS['WSD_A_M_BUTTON_INSTALL'] = 'Установить';
$MESS['WSD_A_M_BUTTON_BACK'] = 'Вернуться к списку';
