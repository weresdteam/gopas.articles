<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

$MESS['WSD_A_M_SUCCESS_TITLE'] = 'Готово!';
$MESS['WSD_A_M_SUCCESS_TEXT'] = 'Модуль удален';
$MESS['WSD_A_M_ERROR_TITLE'] = 'Ошибка!';
$MESS['WSD_A_M_ERROR_TEXT'] = 'Произошла ошибка при удалении модуля!';
$MESS['WSD_A_M_BUTTON_UNINSTALL'] = 'Удалить';
$MESS['WSD_A_M_BUTTON_BACK'] = 'Вернуться к списку';
