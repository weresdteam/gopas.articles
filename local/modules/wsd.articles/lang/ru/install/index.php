<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

$MESS['WSD_A_M_NAME'] = 'Модуль работы со статьями и их структурой.';
$MESS['WSD_A_M_DESCRIPTION'] = 'Позволяет работать с разделами и статьями на базе инфоблоков.';
$MESS['WSD_A_M_INSTALL_TITLE'] = 'Установка модуля статей.';
$MESS['WSD_A_M_UNINSTALL_TITLE'] = 'Удаление модуля статей.';
