<?php
/**
 * Wsd Inc
 * @package wsd
 * @subpackage articles
 * @copyright 2019 Wsd Inc
 */

$MESS['WSD_A_M_FOLDER_TITLE'] = 'Куда установить скопировать компоненты:';
$MESS['WSD_A_M_SAVE_TITLE'] = 'Сохраненное значение при установке:';
$MESS['WSD_A_M_BUTTON_UNINSTALL'] = 'Удалить';
